[![Crates.io](https://img.shields.io/crates/v/crtop.svg?label=crtop)](https://crates.io/crates/crtop)
[![pipeline status](https://gitlab.com/xMAC94x/crtop/badges/master/pipeline.svg)](https://gitlab.com/xMAC94x/crtop/-/pipelines)
[![coverage report](https://gitlab.com/xMAC94x/crtop/badges/master/coverage.svg)](https://gitlab.com/xMAC94x/crtop/-/graphs/master/charts)
[![license](https://img.shields.io/crates/l/crtop)](https://gitlab.com/xMAC94x/crtop/blob/master/LICENSE-MIT)

# crtop - kubernetes Custom Resource TOP

- `kubectl -w` times out after 5 minutes?
- You want to monitor what is happening to a resource in detail
- You want to capture the state of your cluster?

This tool is to improve your daily live as a k8s guy!

## Configuration
Generate a default configuration by using: `crtop generate-config-template`

## Features:
 - [X] Watching CRs
 - [X] Showing History of a CR via diff
 - [X] Show App Logs
 - [X] Delete CR
 - [X] Read Custom Resource Print Fields from CR
 - [X] Recreate CR to last known state
 - [X] Export/Import a session
 - [X] Hide JsonPath from display in history view
 - [X] Edit CRs
 - [ ] Edit Status
 - [ ] Vim support
 - [ ] Combine CR with Operator and show those logs
 - [ ] Harden against Errors With Dialogs rather than crashing
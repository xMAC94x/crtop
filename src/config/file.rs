use crate::config::{
    AdditionalColumn, ClientName, GroupKindVersion, LogInfo, NegativeFilter, ResourceTab, VersionMode, WatchSelector,
};
use serde::{Deserialize, Serialize};
use std::path::PathBuf;

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct ConfigFile {
    pub kubeconfigs: Vec<(ClientName, PathBuf)>,
    pub resources:   Vec<ResourceTab>,
}

impl ConfigFile {
    pub fn generate_default() -> Self {
        Self {
            kubeconfigs: vec![("second_cluster".into(), "/tmp/cluster2.yaml".into())],
            resources:   vec![
                ResourceTab {
                    name: "Pods".into(),
                    group_kind_version: GroupKindVersion {
                        group:   "".to_string(),
                        kind:    "Pod".to_string(),
                        version: VersionMode::Specific("v1".to_string()),
                    },
                    client_name: None,
                    selector: vec![WatchSelector {
                        field_selector: Some("metadata.namespace=default".to_string()),
                        label_selector: None,
                    }],
                    additional_columns: vec![AdditionalColumn {
                        name:      "Status".to_string(),
                        json_path: ".status.phase".to_string(),
                    }],
                    ignored_json_paths: vec![vec!["metadata".to_string(), "managedFields".to_string()], vec![
                        "metadata".to_string(),
                        "resourceVersion".to_string(),
                    ]],
                    logging: vec![LogInfo {
                        name: "dns".into(),
                        operator_label_selector: "k8s-app=kube-dns".to_string(),
                        negative_filter: NegativeFilter {
                            line_regex: "a^".to_string(), // match nothing
                            json_path:  "".to_string(),
                        },
                    }],
                },
                ResourceTab {
                    name: "Deployments".into(),
                    group_kind_version: GroupKindVersion {
                        group:   "apps".to_string(),
                        kind:    "Deployment".to_string(),
                        version: VersionMode::MostStable,
                    },
                    client_name: None,
                    selector: vec![WatchSelector {
                        field_selector: None,
                        label_selector: Some("app.kubernetes.io/managed-by=Helm".to_string()),
                    }],
                    additional_columns: vec![AdditionalColumn {
                        name:      "Ready".to_string(),
                        json_path: ".status.readyReplicas".to_string(),
                    }],
                    ignored_json_paths: vec![vec!["metadata".to_string(), "managedFields".to_string()], vec![
                        "metadata".to_string(),
                        "resourceVersion".to_string(),
                    ]],
                    logging: vec![],
                },
                ResourceTab {
                    name: "Events".into(),
                    group_kind_version: GroupKindVersion {
                        group:   "".to_string(),
                        kind:    "Event".to_string(),
                        version: VersionMode::MostStable,
                    },
                    client_name: Some("second_cluster".into()),
                    selector: vec![WatchSelector {
                        field_selector: None,
                        label_selector: None,
                    }],
                    additional_columns: vec![AdditionalColumn {
                        name:      "Reason".to_string(),
                        json_path: ".reason".to_string(),
                    }],
                    ignored_json_paths: vec![vec!["metadata".to_string(), "managedFields".to_string()], vec![
                        "metadata".to_string(),
                        "resourceVersion".to_string(),
                    ]],
                    logging: vec![],
                },
                ResourceTab {
                    name: "Cert-Manager".into(),
                    group_kind_version: GroupKindVersion {
                        group:   "".to_string(),
                        kind:    "Service".to_string(),
                        version: VersionMode::MostStable,
                    },
                    client_name: None,
                    selector: vec![WatchSelector {
                        field_selector: Some("metadata.name=cert-manager".to_string()),
                        label_selector: None,
                    }],
                    additional_columns: vec![],
                    ignored_json_paths: vec![vec!["metadata".to_string(), "managedFields".to_string()], vec![
                        "metadata".to_string(),
                        "resourceVersion".to_string(),
                    ]],
                    logging: vec![],
                },
            ],
        }
    }
}

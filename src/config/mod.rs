use serde::{Deserialize, Serialize};
use std::fmt::{Debug, Display, Formatter};
pub mod file;

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize, Hash, Eq)]
#[serde(transparent)]
pub struct ResourceTabName(pub String);

#[derive(Default, Clone, Debug, PartialEq, Serialize, Deserialize, Hash, Eq)]
#[serde(transparent)]
pub struct ClientName(pub String);

#[derive(Default, Clone, Debug, PartialEq, Serialize, Deserialize, Hash, Eq)]
#[serde(transparent)]
pub struct LogName(pub String);

/// The NegativeFilter removes Files that MATCH line_regex but NOT match the
/// JSON_PATH value This can be used to filter out operator messages for lines
/// with an other .metadata.name for example
#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct NegativeFilter {
    pub line_regex: String,
    pub json_path:  String,
}

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct LogInfo {
    pub name: LogName,
    /// labelSelector to find the pod(s) that contains the logs
    pub operator_label_selector: String,
    pub negative_filter: NegativeFilter,
}

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct ResourceTab {
    /// must be unique!
    pub name: ResourceTabName,
    /// each tab must have same client and same groupversionkind!
    pub client_name: Option<ClientName>,
    pub group_kind_version: GroupKindVersion,

    pub selector: Vec<WatchSelector>,
    pub additional_columns: Vec<AdditionalColumn>,
    /// a list of json pathes that is ignore on the object, e.g.
    /// ["metadata", "managedFields"] or ["metadata", "resourceVersion"]
    pub ignored_json_paths: Vec<Vec<String>>,
    pub logging: Vec<LogInfo>,
}

#[derive(Clone, Debug, PartialEq, Hash, Eq, Serialize, Deserialize)]
#[serde(untagged)]
pub enum VersionMode {
    MostStable,
    Specific(String),
}

#[derive(Clone, Debug, PartialEq, Hash, Eq, Serialize, Deserialize)]
pub struct GroupKindVersion {
    pub group:   String,
    pub kind:    String,
    pub version: VersionMode,
}

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize, Hash, Eq)]
pub struct WatchSelector {
    pub label_selector: Option<String>,
    pub field_selector: Option<String>,
}

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct AdditionalColumn {
    pub name:      String,
    pub json_path: String,
}

impl<'a> From<&'a str> for ResourceTabName {
    fn from(value: &'a str) -> Self { Self(value.to_string()) }
}
impl<'a> From<&'a str> for ClientName {
    fn from(value: &'a str) -> Self { Self(value.to_string()) }
}
impl<'a> From<&'a str> for LogName {
    fn from(value: &'a str) -> Self { Self(value.to_string()) }
}

impl Display for ResourceTabName {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result { std::fmt::Display::fmt(&self.0, f) }
}
impl Display for ClientName {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result { std::fmt::Display::fmt(&self.0, f) }
}
impl Display for LogName {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result { std::fmt::Display::fmt(&self.0, f) }
}

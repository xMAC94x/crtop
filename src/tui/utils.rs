use crossterm::event::{Event, KeyCode, KeyEvent, KeyEventKind, KeyEventState, KeyModifiers};
use ratatui::prelude::*;
use std::cmp::min;

pub struct Theme {
    pub root:        Style,
    pub borders:     Style,
    pub top_bar:     TopBar,
    pub key_binding: KeyBinding,
    pub menu:        Menu,
    pub resources:   Resources,
    pub history:     History,
}

pub struct KeyBinding {
    pub key:         Style,
    pub description: Style,
    pub background:  Color,
}

pub struct Resources {
    pub tab_default:    Style,
    pub tab_highligted: Style,
    pub tab_background: Style,
    pub row_default:    Style,
    pub row_header:     Style,
    pub row_highligted: Style,
    pub row_deleted:    Style,
}

pub struct History {
    pub time:    Style,
    pub line:    Style,
    pub removed: Style,
    pub added:   Style,
}

pub struct TopBar {
    pub title:      Style,
    pub selected:   Style,
    pub background: Color,
}

pub struct Menu {
    pub default:    Style,
    pub highligted: Style,
    pub background: Color,
}

pub const THEME: Theme = Theme {
    root:        Style::new(),
    borders:     Style::new().fg(LIGHT_GRAY),
    top_bar:     TopBar {
        title:      Style::new().fg(DARK_GRAY).bg(BLACK).add_modifier(Modifier::BOLD),
        selected:   Style::new().fg(MID_GRAY).bg(BLACK),
        background: BLACK,
    },
    key_binding: KeyBinding {
        key:         Style::new().fg(BLACK).bg(DARK_GRAY),
        description: Style::new().fg(DARK_GRAY).bg(BLACK),
        background:  BLACK,
    },
    menu:        Menu {
        default:    Style::new().fg(MID_GRAY).bg(DARK_BLUE),
        highligted: Style::new().fg(LIGHT_GRAY).bg(DARK_BLUE).add_modifier(Modifier::ITALIC),
        background: DARK_BLUE,
    },
    resources:   Resources {
        tab_default:    Style::new().fg(MID_GRAY).bg(DARK_BLUE),
        tab_highligted: Style::new().fg(WHITE).bg(DARK_BLUE).add_modifier(Modifier::BOLD),
        tab_background: Style::new().bg(DARK_BLUE),
        row_default:    Style::new().fg(WHITE),
        row_header:     Style::new().fg(LIGHT_BLUE).add_modifier(Modifier::BOLD),
        row_highligted: Style::new().fg(DARK_BLUE).bg(LIGHT_GRAY).add_modifier(Modifier::BOLD),
        row_deleted:    Style::new().fg(RED),
    },
    history:     History {
        line:    Style::new().fg(WHITE),
        time:    Style::new().fg(LIGHT_BLUE).add_modifier(Modifier::BOLD),
        removed: Style::new().fg(LIGHT_RED),
        added:   Style::new().fg(LIGHT_GREEN),
    },
};

const DARK_BLUE: Color = Color::Rgb(16, 24, 48);
const LIGHT_BLUE: Color = Color::Rgb(64, 96, 192);
//const LIGHT_YELLOW: Color = Color::Rgb(192, 192, 96);
const LIGHT_GREEN: Color = Color::Rgb(64, 192, 96);
const LIGHT_RED: Color = Color::Rgb(192, 96, 96);
const RED: Color = Color::Indexed(160);
const BLACK: Color = Color::Indexed(232); // not really black, often #080808
const DARK_GRAY: Color = Color::Indexed(238);
const MID_GRAY: Color = Color::Indexed(244);
const LIGHT_GRAY: Color = Color::Indexed(250);
const WHITE: Color = Color::Indexed(255); // not really white, often #eeeeee

pub(crate) const fn simple_key(code: KeyCode) -> Event {
    Event::Key(KeyEvent {
        code,
        modifiers: KeyModifiers::NONE,
        kind: KeyEventKind::Press,
        state: KeyEventState::NONE,
    })
}

/// simple helper method to split an area into multiple sub-areas
pub(crate) fn layout(area: Rect, direction: Direction, heights: Vec<u16>) -> std::rc::Rc<[Rect]> {
    let constraints = heights
        .iter()
        .map(|&h| {
            if h > 0 {
                Constraint::Length(h)
            } else {
                Constraint::Min(0)
            }
        })
        .collect::<Vec<_>>();
    Layout::default()
        .direction(direction)
        .constraints(constraints)
        .split(area)
}

pub(crate) fn centered_rect(r: Rect, width: u16, height: u16) -> Rect {
    let top_space = (r.height - height) / 2;
    let bottom_space = r.height - height - top_space;
    let left_space = (r.width - width) / 2;
    let right_space = r.width - width - left_space;

    let vert_layout = Layout::default()
        .direction(Direction::Vertical)
        .constraints([
            Constraint::Length(top_space),
            Constraint::Max(height),
            Constraint::Length(bottom_space),
        ])
        .split(r);
    let middle_layout = Layout::default()
        .direction(Direction::Horizontal)
        .constraints([
            Constraint::Length(left_space),
            Constraint::Max(width),
            Constraint::Length(right_space),
        ])
        .split(vert_layout[1]);

    let mut bounds_checked = middle_layout[1];
    bounds_checked.width = min(bounds_checked.width, r.width);
    bounds_checked.height = min(bounds_checked.height, r.height);

    bounds_checked
}

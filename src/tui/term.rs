use std::{
    io::{self, Stdout},
    ops::{Deref, DerefMut},
    sync::Mutex,
};

use anyhow::{Context, Result};
use crossterm::{
    event::{DisableMouseCapture, EnableMouseCapture},
    execute,
    terminal::{disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen},
};
use ratatui::prelude::*;

/// A wrapper around the terminal that handles setting up and tearing down the
/// terminal and provides a helper method to read events from the terminal.
#[derive(Debug)]
pub struct Term {
    pub terminal: Terminal<CrosstermBackend<Stdout>>,
}

static COPY_MODE_ENABLED: Mutex<bool> = Mutex::new(false);

impl Term {
    pub fn start() -> Result<Self> {
        // this size is to match the size of the terminal when running the demo
        // using vhs in a 1280x640 sized window (github social preview size)
        let options = TerminalOptions {
            viewport: Viewport::Fullscreen,
        };
        let mut terminal = Terminal::with_options(CrosstermBackend::new(io::stdout()), options)?;
        enable_raw_mode().context("enable raw mode")?;
        execute!(terminal.backend_mut(), EnterAlternateScreen).context("enter alternate screen")?;
        Ok(Self { terminal })
    }

    pub fn stop(&mut self) -> Result<()> {
        if *COPY_MODE_ENABLED.lock().unwrap() {
            execute!(self.terminal.backend_mut(), DisableMouseCapture)?;
        }
        disable_raw_mode().context("disable raw mode")?;
        execute!(self.terminal.backend_mut(), LeaveAlternateScreen).context("leave alternate screen")?;
        Ok(())
    }

    pub fn toggle_copy_mode(&mut self) -> Result<()> {
        let copy_mode = !*COPY_MODE_ENABLED.lock().unwrap();
        if copy_mode {
            execute!(self.terminal.backend_mut(), EnableMouseCapture)?;
        } else {
            execute!(self.terminal.backend_mut(), DisableMouseCapture)?;
        }
        *COPY_MODE_ENABLED.lock().unwrap() = copy_mode;
        Ok(())
    }
}

impl Deref for Term {
    type Target = Terminal<CrosstermBackend<Stdout>>;

    fn deref(&self) -> &Self::Target { &self.terminal }
}

impl DerefMut for Term {
    fn deref_mut(&mut self) -> &mut Self::Target { &mut self.terminal }
}

impl Drop for Term {
    fn drop(&mut self) { let _ = self.stop(); }
}

use std::{error::Error, slice::Iter, sync::Arc, time::Duration};

use crate::{
    k8s::{clients::Clients, discovery::Discovery, fetcher::Fetcher},
    tui::{
        handler::{append_modifier, get_action, ActionDesc, FixedActionDesc, Handler},
        screens::{
            AppLogs, DetailedActions, DetailedActionsAction, DetailedActionsResult, LoadSaveStore, LoadSaveStoreAction,
            Menu, MenuAction, MenuResult, Resources, ResourcesNestedAction,
        },
    },
};
use crossterm::event::{self, Event, KeyCode, KeyModifiers};
use ratatui::{prelude::*, widgets::*};
use term::Term;
use tokio::runtime::Runtime;
use utils::*;

mod handler;
mod list;
mod screens;
mod term;
pub mod trace;
mod utils;

pub enum TopLevelScreen {
    Resources,
    AppLogs,
    LoadStore,
}

impl TopLevelScreen {
    #[allow(unused)]
    fn desc(&self) -> &'static str {
        match self {
            TopLevelScreen::Resources => "Resources",
            TopLevelScreen::AppLogs => "AppLogs",
            TopLevelScreen::LoadStore => "Load/Store",
        }
    }
}

pub struct App {
    top_level: TopLevelScreen,
    term: Option<Term>,
    should_run: bool,
    menu: Menu,
    show_menu: bool,
    detailed_actions: DetailedActions,
    show_detailed_actions: bool,
    resources: Resources,
    app_logs: AppLogs,
    loadstore: LoadSaveStore,
}

pub struct AppState {
    pub runtime:   Arc<Runtime>,
    pub clients:   Clients,
    pub discovery: Discovery,
    pub fetcher:   Fetcher,
}

impl App {
    pub fn new(state: AppState) -> Self {
        let fetcher = Arc::new(state.fetcher);
        Self {
            top_level: TopLevelScreen::Resources,
            term: Some(Term::start().unwrap()),
            should_run: true,
            menu: Menu::default(),
            show_menu: false,
            detailed_actions: DetailedActions::new(vec![]),
            show_detailed_actions: false,
            resources: Resources::new(
                Arc::clone(&state.runtime),
                Arc::clone(&fetcher),
                Arc::new(state.clients),
                Arc::new(std::sync::Mutex::new(state.discovery)),
            ),
            app_logs: AppLogs::default(),
            loadstore: LoadSaveStore::new(Arc::clone(&state.runtime), Arc::clone(&fetcher)),
        }
    }

    pub fn run(mut self) -> Result<(), Box<dyn Error>> {
        self.should_run = true;
        const MAXIMAL_SLEEP: Duration = Duration::from_millis(50);

        while self.should_run {
            let mut term = self.term.take().unwrap();
            term.draw(|f| f.render_widget(&self, f.size()))?;
            self.term = Some(term);

            /*
            {
                if let Ok(store) = state.fetcher.store().try_read() {
                    let state = ScreenState { store: &store };
                    self.handle_state(&state);
                }
            }*/

            if event::poll(MAXIMAL_SLEEP)? {
                let e = event::read()?;
                if let Event::Resize(width, height) = &e {
                    let mut term = self.term.take().unwrap();
                    println!("dasd");
                    term.resize(Rect::new(0, 0, *width, *height))?;
                    self.term = Some(term);
                }
                if let Some(mut action) = get_action(&self, e) {
                    self.handle_action(&mut action)?
                }
            }
        }
        Ok(())
    }

    pub fn render_top_bar(&self, area: Rect, buf: &mut Buffer) {
        let area = layout(area, Direction::Horizontal, vec![6, 0, 30]);

        let spans = vec![Span::styled("crtop".to_string(), THEME.top_bar.title)];
        Paragraph::new(Line::from(spans))
            .alignment(Alignment::Left)
            .bg(THEME.top_bar.background)
            .render(area[0], buf);
        Block::new()
            .style(Style::new().bg(THEME.top_bar.background))
            .render(area[1], buf);

        let spans = vec![Span::styled(self.top_level.desc().to_string(), THEME.top_bar.selected)];
        Paragraph::new(Line::from(spans))
            .alignment(Alignment::Right)
            .bg(THEME.top_bar.background)
            .render(area[2], buf);
    }

    pub fn render_bottom_bar(&self, area: Rect, buf: &mut Buffer) {
        let spanify = |actions: Iter<ActionDesc<AppNestedAction>>| {
            actions
                .filter_map(|e| match e.printable_key() {
                    Some(key) => {
                        let key = Span::styled(format!(" {key} "), THEME.key_binding.key);
                        let desc = Span::styled(format!(" {} ", e.title()), THEME.key_binding.description);
                        Some([key, desc])
                    },
                    None => None,
                })
                .flatten()
                .collect::<Vec<_>>()
        };
        let actions = self.promoted_actions();
        let (first, last) = actions.split_at((actions.len() / 2).max(6).min(actions.len()));
        let spans = [spanify(first.iter()), spanify(last.iter())];
        Paragraph::new(spans.map(Line::from).into_iter().collect::<Vec<_>>())
            .alignment(Alignment::Center)
            .bg(THEME.key_binding.background)
            .render(area, buf);
    }
}

impl Widget for &App {
    fn render(self, area: Rect, buf: &mut Buffer) {
        Block::new().style(THEME.root).render(area, buf);
        let area = layout(area, Direction::Vertical, vec![1, 0, 2]);

        self.render_top_bar(area[0], buf);
        match self.top_level {
            TopLevelScreen::Resources => self.resources.render(area[1], buf),
            TopLevelScreen::AppLogs => self.app_logs.render(area[1], buf),
            TopLevelScreen::LoadStore => self.loadstore.render(area[1], buf),
        }
        if self.show_menu {
            self.menu.render(area[1], buf);
        }
        if self.show_detailed_actions {
            self.detailed_actions.render(area[1], buf);
        }
        self.render_bottom_bar(area[2], buf);
    }
}

#[derive(Clone)]
pub enum AppAction {
    Close,
    ToggleCopyMode,
    DetailedHelp,
    Menu,
}

#[derive(Clone)]
pub enum AppNestedAction {
    App(AppAction),
    Menu(MenuAction),
    DetailedActions(DetailedActionsAction),
    Resources(ResourcesNestedAction),
    LoadStore(LoadSaveStoreAction),
}

impl Handler for App {
    type Action = AppNestedAction;
    type Error = Box<dyn Error>;
    type Result = ();

    fn promoted_actions(&self) -> Vec<ActionDesc<Self::Action>> {
        let close = FixedActionDesc {
            action:      AppNestedAction::App(AppAction::Close),
            events:      vec![(simple_key(KeyCode::Char('q')), "q")],
            title:       "quit",
            description: "closes the application",
        }
        .into();

        let copy_mode = FixedActionDesc {
            action:      AppNestedAction::App(AppAction::ToggleCopyMode),
            events:      vec![(simple_key(KeyCode::Char('p')), "p")],
            title:       "copy mode",
            description: "toggles copy mode, when enabled the terminal no longer catches the cursor and you can copy \
                          paste code from it",
        }
        .into();

        let details = FixedActionDesc {
            action:      AppNestedAction::App(AppAction::DetailedHelp),
            events:      vec![(simple_key(KeyCode::Char('h')), "h")],
            title:       "help",
            description: "show detailed help information for each action",
        }
        .into();

        let menu = FixedActionDesc {
            action:      AppNestedAction::App(AppAction::Menu),
            events:      vec![(simple_key(KeyCode::Char('m')), "m")],
            title:       "menu",
            description: "switching to another screen",
        }
        .into();

        let top_level = match self.top_level {
            TopLevelScreen::Resources => map_actions(self.resources.promoted_actions(), AppNestedAction::Resources),
            TopLevelScreen::AppLogs => vec![],
            TopLevelScreen::LoadStore => map_actions(self.loadstore.promoted_actions(), AppNestedAction::LoadStore),
        };

        if self.show_detailed_actions {
            return map_actions(
                self.detailed_actions.promoted_actions(),
                AppNestedAction::DetailedActions,
            );
        }

        if self.show_menu {
            let mut res = map_actions(self.menu.promoted_actions(), AppNestedAction::Menu);
            res.append(&mut vec![details]);
            res
        } else if top_level.iter().any(|a| matches!(a, ActionDesc::Dynamic(_))) {
            let mut res = top_level;
            let mut actions = vec![close, copy_mode, details, menu];
            append_modifier(&mut actions, KeyModifiers::ALT);
            res.append(&mut actions);
            res
        } else {
            let mut res = top_level;
            res.append(&mut vec![close, copy_mode, details, menu]);
            res
        }
    }

    fn handle_action(&mut self, action: &mut Self::Action) -> Result<(), Self::Error> {
        match action {
            AppNestedAction::App(AppAction::Close) => self.should_run = false,
            AppNestedAction::App(AppAction::ToggleCopyMode) => {
                if let Some(term) = &mut self.term {
                    term.toggle_copy_mode()?;
                }
            },
            AppNestedAction::App(AppAction::Menu) => self.show_menu = true,
            AppNestedAction::App(AppAction::DetailedHelp) => {
                let desc = self
                    .promoted_actions()
                    .iter()
                    .filter_map(|d| {
                        d.printable_key()
                            .map(|key| format!("{} - {} - {key}", d.title(), d.description()))
                    })
                    .collect();
                self.detailed_actions = DetailedActions::new(desc);
                self.show_detailed_actions = true
            },
            AppNestedAction::Menu(m) => match self.menu.handle_action(m)? {
                Some(MenuResult::Close) => self.show_menu = false,
                Some(MenuResult::Select(screen)) => {
                    self.top_level = screen;
                    self.show_menu = false;
                },
                None => (),
            },
            #[allow(clippy::single_match)]
            AppNestedAction::DetailedActions(d) => match self.detailed_actions.handle_action(d)? {
                Some(DetailedActionsResult::Close) => self.show_detailed_actions = false,
                None => (),
            },
            AppNestedAction::Resources(r) => self.resources.handle_action(r)?,
            AppNestedAction::LoadStore(l) => {
                if self.loadstore.handle_action(l)? {
                    self.top_level = TopLevelScreen::Resources
                }
            },
        };
        Ok(())
    }
}

fn map_actions<T, T2, F>(list: Vec<ActionDesc<T>>, f: F) -> Vec<ActionDesc<T2>>
where
    F: Fn(T) -> T2 + Copy + 'static,
    T: Clone + 'static,
    T2: Clone,
{
    list.into_iter().map(|ed| ed.map_action(f)).collect()
}

/*
fn map_actions<T, O, F>(list: Vec<FixedActionDesc<T>>, f: F) -> Vec<FixedActionDesc<O>>
where
    F: Fn(T) -> O + Copy,
{
    list.into_iter().map(|ed| ed.map_action(f)).collect()
}
 */

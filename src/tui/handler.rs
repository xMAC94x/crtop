use crossterm::{
    event,
    event::{Event, KeyModifiers},
};

pub struct FixedActionDesc<T> {
    pub action:      T,
    // List of all Events trigger that, including the respective key in human readable format
    pub events:      Vec<(event::Event, &'static str)>,
    pub title:       &'static str,
    pub description: &'static str,
}

pub struct DynamicActionDesc<T> {
    pub try_from_event: Box<dyn Fn(event::Event) -> Result<T, event::Event>>,
    pub title: &'static str,
    pub description: &'static str,
}

pub(crate) enum ActionDesc<T> {
    Fixed(FixedActionDesc<T>),
    Dynamic(DynamicActionDesc<T>),
}

impl<T> ActionDesc<T>
where
    T: Clone,
{
    fn try_from_event(&self, event: event::Event) -> Result<T, event::Event> {
        match self {
            ActionDesc::Fixed(fixed) => {
                for e in &fixed.events {
                    if e.0 == event {
                        return Ok(fixed.action.clone());
                    }
                }
                Err(event)
            },
            ActionDesc::Dynamic(dynamic) => (dynamic.try_from_event)(event),
        }
    }

    pub fn title(&self) -> &'static str {
        match self {
            ActionDesc::Fixed(fixed) => fixed.title,
            ActionDesc::Dynamic(dynamic) => dynamic.title,
        }
    }

    pub fn description(&self) -> &'static str {
        match self {
            ActionDesc::Fixed(fixed) => fixed.description,
            ActionDesc::Dynamic(dynamic) => dynamic.description,
        }
    }

    pub fn printable_key(&self) -> Option<String> {
        match self {
            ActionDesc::Fixed(fixed) => Some(
                fixed
                    .events
                    .iter()
                    .map(|(_, k)| k)
                    .fold(String::new(), |a, b| format!("{a}/{b}"))
                    .split_off(1),
            ),
            ActionDesc::Dynamic(_) => None,
        }
    }

    pub fn map_action<T2, F>(self, f: F) -> ActionDesc<T2>
    where
        F: FnOnce(T) -> T2 + Copy + 'static,
        T: Clone + 'static,
    {
        match self {
            ActionDesc::Fixed(fixed) => ActionDesc::Fixed(FixedActionDesc::<T2> {
                action:      f(fixed.action),
                events:      fixed.events,
                title:       fixed.title,
                description: fixed.description,
            }),
            ActionDesc::Dynamic(mut dynamic) => ActionDesc::Dynamic(DynamicActionDesc::<T2> {
                try_from_event: {
                    let try_from_event = std::mem::replace(&mut dynamic.try_from_event, Box::new(noop));
                    Box::new(move |e| match (try_from_event)(e) {
                        Ok(a) => Ok(f(a)),
                        Err(e) => Err(e),
                    })
                },
                title: dynamic.title,
                description: dynamic.description,
            }),
        }
    }
}

fn noop<T>(e: event::Event) -> Result<T, event::Event> { Err(e) }

impl<T> From<FixedActionDesc<T>> for ActionDesc<T> {
    fn from(value: FixedActionDesc<T>) -> Self { ActionDesc::Fixed(value) }
}
impl<T> From<DynamicActionDesc<T>> for ActionDesc<T> {
    fn from(value: DynamicActionDesc<T>) -> Self { ActionDesc::Dynamic(value) }
}

/// A window a part of the current drawn screen. For example there exist the
/// default window, or the "Do you want to exit window". A window can overshadow
/// another window, still drawing its content but no longer taking commands
pub trait Handler {
    type Action;
    type Result;
    type Error;
    /// All actions that should be display in help
    fn promoted_actions(&self) -> Vec<ActionDesc<Self::Action>>;

    fn handle_action(&mut self, action: &mut Self::Action) -> Result<Self::Result, Self::Error>;
}

pub fn get_action<T: Handler>(handler: &T, mut event: event::Event) -> Option<T::Action>
where
    T::Action: Clone,
{
    let actions = handler.promoted_actions();
    // first evaluate fixed, then dynamic

    for description in actions.iter().filter(|desc| matches!(desc, ActionDesc::Fixed(_))) {
        event = match description.try_from_event(event) {
            Ok(a) => return Some(a),
            Err(event) => event,
        }
    }
    for description in actions.iter().filter(|desc| matches!(desc, ActionDesc::Dynamic(_))) {
        event = match description.try_from_event(event) {
            Ok(a) => return Some(a),
            Err(event) => event,
        }
    }
    None
}

pub fn append_modifier<T>(actions: &mut Vec<ActionDesc<T>>, add_modifiers: KeyModifiers) {
    for action_desc in actions {
        #[allow(clippy::single_match)]
        match action_desc {
            ActionDesc::Fixed(FixedActionDesc { events, .. }) => {
                for (event, _) in events {
                    if let Event::Key(key) = event {
                        key.modifiers.insert(add_modifiers);
                    }
                }
            },
            _ => (),
        }
    }
}

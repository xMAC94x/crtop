use ratatui::{
    prelude::{Style, Text},
    text::Line,
};
use std::{
    io::{self, Write},
    sync::{Arc, Mutex},
};
use tracing::{info, warn};
use tracing_appender::non_blocking::WorkerGuard;
use tracing_subscriber::{filter::LevelFilter, fmt::writer::MakeWriter, prelude::*, registry, EnvFilter};

const RUST_LOG_ENV: &str = "RUST_LOG";

pub fn init<W>(terminal: &'static W) -> Vec<impl Drop>
where
    W: MakeWriter<'static> + 'static,
    <W as MakeWriter<'static>>::Writer: 'static + Send + Sync,
{
    let mut guards: Vec<WorkerGuard> = Vec::new();
    let mut filter = EnvFilter::default().add_directive(LevelFilter::INFO.into());
    let default_directives = [
        "hyper=info",
        "mio::poll=info",
        "mio::sys::windows=info",
        "tower::buffer::worker=info",
        "tower::buffer::service=info",
        "tokio_util=info",
    ];
    for s in default_directives {
        filter = filter.add_directive(s.parse().unwrap());
    }
    match std::env::var(RUST_LOG_ENV) {
        Ok(env) => {
            for s in env.split(',') {
                match s.parse() {
                    Ok(d) => filter = filter.add_directive(d),
                    Err(err) => eprintln!("WARN ignoring log directive: `{s}`: {err}"),
                }
            }
        },
        Err(std::env::VarError::NotUnicode(os_string)) => {
            eprintln!("WARN ignoring log directives due to non-unicode data: {os_string:?}");
        },
        Err(std::env::VarError::NotPresent) => {},
    };
    let filter = filter; // mutation is done
    let registry = registry();
    let registry = {
        let (non_blocking, stdio_guard) = tracing_appender::non_blocking(terminal.make_writer());
        guards.push(stdio_guard);
        registry.with(tracing_subscriber::fmt::layer().with_writer(non_blocking))
    };
    registry.with(filter).init();
    if tracing::level_enabled!(tracing::Level::TRACE) {
        info!("Tracing Level: TRACE");
    } else if tracing::level_enabled!(tracing::Level::DEBUG) {
        info!("Tracing Level: DEBUG");
    };
    guards
}

#[derive(Debug, Default, Clone)]
pub struct TuiLog<'a> {
    pub inner: Arc<Mutex<Text<'a>>>,
}

impl<'a> TuiLog<'a> {
    pub fn drain(&self, height: usize) -> u16 {
        let mut inner = self.inner.lock().unwrap();

        let len = inner.lines.len().saturating_sub(height + 5000);
        inner.lines.drain(..len);
        // TODO: account for line wraping
        inner.lines.len().saturating_sub(height) as u16
    }
}

impl<'a> Write for TuiLog<'a> {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        // TODO: this processing can probably occur in the consumer of the log lines
        // (and instead of having a TuiLog::resize the consumer can take
        // ownership of the lines and manage them itself).

        // Not super confident this is the ideal parser but it works for now and doesn't
        // depend on an old version of nom. Alternatives to consider may include
        // `vte`, `anstyle-parse`, `vt100`, or others.
        use cansi::v3::categorise_text;
        use ratatui::{
            style::{Color, Modifier},
            text::Span,
        };

        let line = core::str::from_utf8(buf).map_err(|e| io::Error::new(io::ErrorKind::InvalidData, e))?;

        let mut spans = Vec::new();
        let mut lines = Vec::new();

        for out in categorise_text(line) {
            let mut style = Style::default();
            // NOTE: There are other values returned from cansi that we don't bother to use
            // for now including background color, italics, blinking, etc.
            style.fg = match out.fg {
                Some(cansi::Color::Black) => Some(Color::Black),
                Some(cansi::Color::Red) => Some(Color::Red),
                Some(cansi::Color::Green) => Some(Color::Green),
                Some(cansi::Color::Yellow) => Some(Color::Yellow),
                Some(cansi::Color::Blue) => Some(Color::Blue),
                Some(cansi::Color::Magenta) => Some(Color::Magenta),
                Some(cansi::Color::Cyan) => Some(Color::Cyan),
                Some(cansi::Color::White) => Some(Color::White),
                // "Bright" versions currently not handled
                Some(c) => {
                    warn!("Unknown color {:#?}", c);
                    style.fg
                },
                None => style.fg,
            };
            match out.intensity {
                Some(cansi::Intensity::Normal) | None => {},
                Some(cansi::Intensity::Bold) => style.add_modifier = Modifier::BOLD,
                Some(cansi::Intensity::Faint) => style.add_modifier = Modifier::DIM,
            }

            // search for newlines
            for t in out.text.split_inclusive('\n') {
                if !t.is_empty() {
                    spans.push(Span::styled(t.to_owned(), style));
                }
                if t.ends_with('\n') {
                    lines.push(Line::from(core::mem::take(&mut spans)));
                }
            }
        }
        if !spans.is_empty() {
            lines.push(Line::from(spans));
        }

        self.inner.lock().unwrap().lines.append(&mut lines);

        Ok(buf.len())
    }

    // We can potentially use this to reduce locking frequency?
    fn flush(&mut self) -> io::Result<()> { Ok(()) }
}

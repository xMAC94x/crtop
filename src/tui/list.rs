use ratatui::{prelude::Style, text::Line, widgets::*};
use std::fmt::Display;

#[derive(Default, Clone)]
#[allow(dead_code)]
pub enum ListOverflowBehavior {
    #[default]
    Stop,
    Overflow,
    Unselect,
}

#[derive(Default, Clone)]
pub struct ListOption {
    /// behavior when going before the first element
    pub underflow:  ListOverflowBehavior,
    /// behavior when going after the last element
    pub overflow:   ListOverflowBehavior,
    /// if set, we always try to select an alement, if an element exists.
    pub always_one: bool,
}

pub struct ListData<T> {
    elements: Vec<T>,
    offset:   usize,
    selected: Option<usize>,
    options:  ListOption,
}

impl<T> ListData<T> {
    pub fn new(elements: Vec<T>, options: &ListOption) -> Self {
        let mut s = Self {
            elements,
            options: options.clone(),
            offset: 0,
            selected: None,
        };
        s.select_reset();
        s
    }

    pub fn update_elements_inplace(&mut self, new: Vec<T>)
    where
        T: PartialEq,
    {
        let old_elemts = std::mem::replace(&mut self.elements, new);
        let selected = self.selected.and_then(|i| old_elemts.get(i));
        if let Some(selected) = selected {
            let new_pos = self.elements.iter().position(|e| e == selected);
            if let Some(new_pos) = new_pos {
                self.select(Some(new_pos));
                return;
            }
        }
        self.select_reset();
    }

    pub fn listitems(&self, style: Style) -> Vec<ListItem<'static>>
    where
        T: Display,
    {
        self.elements
            .iter()
            .map(|e| ListItem::new(format!("{}", e)).style(style))
            .collect()
    }

    pub fn lineitems(&self, style: Style) -> Vec<Line<'static>>
    where
        T: Display,
    {
        self.elements
            .iter()
            .map(|e| Line::styled(format!("{}", e), style))
            .collect()
    }

    pub fn elements(&self) -> &Vec<T> { &self.elements }

    pub fn liststate(&mut self) -> ListState {
        ListState::default()
            .with_selected(self.selected)
            .with_offset(self.offset)
    }

    pub fn update_from_liststate(&mut self, state: ListState) {
        self.selected = state.selected();
        self.offset = state.offset();
    }

    pub fn tablestate(&mut self) -> TableState {
        TableState::default()
            .with_selected(self.selected)
            .with_offset(self.offset)
    }

    pub fn update_from_tablestate(&mut self, state: TableState) {
        self.selected = state.selected();
        self.offset = state.offset();
    }

    pub fn previous(&mut self) {
        let index = self.selected.unwrap_or(1);
        let new_index = if index == 0 {
            match self.options.underflow {
                ListOverflowBehavior::Stop => Some(0),
                ListOverflowBehavior::Overflow => Some(self.elements.len()),
                ListOverflowBehavior::Unselect => None,
            }
        } else {
            Some(index - 1)
        };
        self.select(new_index);
    }

    pub fn next(&mut self) {
        let index = self.selected.unwrap_or_default();
        let len = self.elements.len();
        let new_index = if index == len - 1 {
            match self.options.overflow {
                ListOverflowBehavior::Stop => Some(len - 1),
                ListOverflowBehavior::Overflow => Some(0),
                ListOverflowBehavior::Unselect => None,
            }
        } else {
            Some(index + 1)
        };
        self.select(new_index);
    }

    pub fn selected(&self) -> Option<&T> { self.selected.and_then(|i| self.elements.get(i)) }

    pub fn selected_index(&self) -> Option<usize> { self.selected }

    /// tries to follow the "always select one pattern"
    fn select_reset(&mut self) {
        if self.options.always_one && self.elements.first().is_some() {
            self.select(Some(0))
        } else {
            self.select(None); // reset
        }
    }

    /// tries to keep offset so that selected is seeable
    fn select(&mut self, index: Option<usize>) {
        self.selected = index;
        if index.is_none() {
            self.offset = 0;
        }
    }
}

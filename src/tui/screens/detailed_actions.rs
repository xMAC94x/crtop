use crate::tui::{
    handler::{ActionDesc, FixedActionDesc, Handler},
    utils::*,
};
use crossterm::event::KeyCode;
use ratatui::{prelude::*, widgets::*};
use std::error::Error;

pub struct DetailedActions {
    descriptions: Vec<String>,
}

impl DetailedActions {
    pub(crate) fn new(descriptions: Vec<String>) -> Self { Self { descriptions } }
}

impl Widget for &DetailedActions {
    fn render(self, area: Rect, buf: &mut Buffer) {
        let height = self.descriptions.len() as u16;
        let width = self
            .descriptions
            .iter()
            .max_by_key(|e| e.len())
            .map(|e| e.len())
            .unwrap_or_default() as u16;

        const MARGIN: Margin = Margin {
            horizontal: 2,
            vertical:   2,
        };

        let center = centered_rect(area, width + 2 + MARGIN.horizontal * 2, height + MARGIN.vertical * 2);

        let listitems: Vec<_> = self
            .descriptions
            .iter()
            .map(|e| ListItem::new(e.to_string()).style(THEME.menu.highligted))
            .collect();

        Clear.render(center, buf);
        Block::default()
            .bg(THEME.menu.background)
            .borders(Borders::all())
            .border_style(THEME.borders)
            .title("Help")
            .render(center, buf);
        let inner_center = center.inner(&MARGIN);

        let tuilist = List::new(listitems)
            .highlight_symbol("> ")
            .highlight_style(THEME.menu.highligted)
            .direction(ListDirection::TopToBottom);

        ratatui::widgets::Widget::render(tuilist, inner_center, buf);
    }
}

#[derive(Clone)]
pub enum DetailedActionsAction {
    Close,
}

pub enum DetailedActionsResult {
    Close,
}

impl Handler for DetailedActions {
    type Action = DetailedActionsAction;
    type Error = Box<dyn Error>;
    type Result = Option<DetailedActionsResult>;

    fn promoted_actions(&self) -> Vec<ActionDesc<Self::Action>> {
        let close = FixedActionDesc {
            action:      DetailedActionsAction::Close,
            events:      vec![(simple_key(KeyCode::Esc), "ESC")],
            title:       "close help",
            description: "closes this detailed action help",
        }
        .into();

        vec![close]
    }

    fn handle_action(&mut self, action: &mut Self::Action) -> Result<Self::Result, Self::Error> {
        match action {
            DetailedActionsAction::Close => Ok(Some(DetailedActionsResult::Close)),
        }
    }
}

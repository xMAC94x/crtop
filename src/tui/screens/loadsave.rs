use crate::{
    k8s::fetcher::Fetcher,
    tui::{
        handler::{ActionDesc, DynamicActionDesc, FixedActionDesc, Handler},
        utils::*,
    },
};
use crossterm::event::{Event, KeyCode, KeyEvent};
use ratatui::{prelude::*, widgets::*};
use std::{error::Error, io::BufReader, sync::Arc};
use tokio::runtime::Runtime;

pub enum LoadSaveMode {
    Load,
    Save,
    Reset,
}

pub struct LoadSaveStore {
    runtime:  Arc<Runtime>,
    fetcher:  Arc<Fetcher>,
    filename: String,
    mode:     LoadSaveMode,
}

impl LoadSaveMode {
    pub fn next(&mut self) {
        match self {
            LoadSaveMode::Load => *self = LoadSaveMode::Save,
            LoadSaveMode::Save => *self = LoadSaveMode::Reset,
            LoadSaveMode::Reset => *self = LoadSaveMode::Load,
        }
    }
}

impl LoadSaveStore {
    pub(crate) fn new(runtime: Arc<Runtime>, fetcher: Arc<Fetcher>) -> Self {
        Self {
            runtime,
            fetcher,
            filename: home::home_dir()
                .map(|mut f| {
                    f.push("crtop_export.json");
                    f.as_os_str().to_string_lossy().to_string()
                })
                .unwrap_or("/tmp/export.json".to_string()),
            mode: LoadSaveMode::Load,
        }
    }
}

impl Widget for &LoadSaveStore {
    fn render(self, area: Rect, buf: &mut Buffer) {
        let height = 15_u16;
        let width = (self.filename.len() as u16 + 8).max(30);

        const OUTER_MARGIN: Margin = Margin {
            horizontal: 2,
            vertical:   2,
        };

        let center = centered_rect(
            area,
            width + 2 + OUTER_MARGIN.horizontal * 2,
            height + OUTER_MARGIN.vertical * 2,
        );

        Clear.render(center, buf);
        Block::default()
            .bg(THEME.menu.background)
            .borders(Borders::all())
            .border_style(THEME.borders)
            .title("In/Export of Store")
            .render(center, buf);

        let layout = Layout::default()
            .direction(Direction::Vertical)
            .constraints([
                Constraint::Length(3),
                Constraint::Min(3),
                Constraint::Length(3),
                Constraint::Length(3),
                Constraint::Length(3),
            ])
            .split(center.inner(&OUTER_MARGIN));

        let border = Block::default().borders(Borders::all()).border_style(THEME.borders);

        Paragraph::new(self.filename.clone())
            .bg(THEME.menu.background)
            .block(border.clone())
            .render(layout[0], buf);

        let btn_layout = Layout::default()
            .direction(Direction::Horizontal)
            .horizontal_margin(4)
            .constraints([Constraint::Length(10)]);

        let button1 = btn_layout.split(layout[2])[0];
        let button2 = btn_layout.split(layout[3])[0];
        let button3 = btn_layout.split(layout[4])[0];

        let (load, save, reset) = match &self.mode {
            LoadSaveMode::Load => (
                Span::styled(">> load <<", Style::new().bold()),
                Span::raw("save"),
                Span::raw("reset"),
            ),
            LoadSaveMode::Save => (
                Span::raw("load"),
                Span::styled(">> save <<", Style::new().bold()),
                Span::raw("reset"),
            ),
            LoadSaveMode::Reset => (
                Span::raw("load"),
                Span::raw("save"),
                Span::styled(">> reset <<", Style::new().bold()),
            ),
        };

        Paragraph::new(load)
            .bg(THEME.menu.background)
            .alignment(Alignment::Center)
            .block(border.clone())
            .render(button1, buf);

        Paragraph::new(save)
            .bg(THEME.menu.background)
            .alignment(Alignment::Center)
            .block(border.clone())
            .render(button2, buf);

        Paragraph::new(reset)
            .bg(THEME.menu.background)
            .alignment(Alignment::Center)
            .block(border)
            .render(button3, buf);
    }
}

#[derive(Clone)]
pub enum LoadSaveStoreAction {
    Abort,
    Switch,
    EditText(KeyEvent),
    Confirm,
}

impl Handler for LoadSaveStore {
    type Action = LoadSaveStoreAction;
    type Error = Box<dyn Error>;
    type Result = bool;

    fn promoted_actions(&self) -> Vec<ActionDesc<Self::Action>> {
        let abort = FixedActionDesc {
            action:      LoadSaveStoreAction::Abort,
            events:      vec![(simple_key(KeyCode::Esc), "ESC")],
            title:       "abort",
            description: "closes this load save selection",
        }
        .into();

        let (title, description) = match self.mode {
            LoadSaveMode::Load => ("load", "load data from file"),
            LoadSaveMode::Save => ("save", "stores data to file"),
            LoadSaveMode::Reset => ("reset", "resets all data in resource tag"),
        };

        let confirm = FixedActionDesc {
            action: LoadSaveStoreAction::Confirm,
            events: vec![(simple_key(KeyCode::Enter), "Enter")],
            title,
            description,
        }
        .into();

        let switch = FixedActionDesc {
            action:      LoadSaveStoreAction::Switch,
            events:      vec![(simple_key(KeyCode::Tab), "Tab")],
            title:       "toggle",
            description: "toggle between save and load",
        }
        .into();

        let edit = DynamicActionDesc {
            try_from_event: Box::new(edit_text),
            title: "<TYPING>",
            description: "type anything to select file",
        }
        .into();

        vec![abort, switch, confirm, edit]
    }

    fn handle_action(&mut self, action: &mut Self::Action) -> Result<Self::Result, Self::Error> {
        match action {
            LoadSaveStoreAction::Abort => Ok(true),
            LoadSaveStoreAction::Confirm => {
                use crate::k8s::tab_store::TabStore;
                use std::{fs::File, io::prelude::*};
                let binding = self.fetcher.store();
                match self.mode {
                    LoadSaveMode::Load => {
                        let file = File::open(&self.filename)?;
                        let reader = BufReader::new(file);
                        let data: TabStore = serde_json::from_reader(reader)?;
                        let mut store = self.runtime.block_on(binding.write());
                        store.append(data);
                        tracing::info!(?self.filename, "Loaded data from disk")
                    },
                    LoadSaveMode::Save => {
                        let data = {
                            let store = self.runtime.block_on(binding.read());
                            serde_json::to_string(&(*store))?
                        };
                        let mut file = File::create(&self.filename)?;
                        file.write_all(data.as_bytes())?;
                        tracing::info!(?self.filename, "Stored data to disk")
                    },
                    LoadSaveMode::Reset => {
                        let mut store = self.runtime.block_on(binding.write());
                        *store = TabStore::default();
                    },
                }
                Ok(true)
            },
            LoadSaveStoreAction::Switch => {
                self.mode.next();
                Ok(false)
            },
            LoadSaveStoreAction::EditText(e) => {
                if e.code == KeyCode::Backspace {
                    let mut chars = self.filename.chars();
                    chars.next_back();
                    self.filename = chars.as_str().to_string()
                } else if let KeyCode::Char(c) = e.code {
                    self.filename = format!("{}{}", self.filename, c)
                }
                Ok(false)
            },
        }
    }
}

fn edit_text(e: Event) -> Result<LoadSaveStoreAction, Event> {
    if let Event::Key(ke) = e {
        Ok(LoadSaveStoreAction::EditText(ke))
    } else {
        Err(e)
    }
}

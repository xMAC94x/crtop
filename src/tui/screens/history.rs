use crate::{
    config::ResourceTabName,
    k8s::{fetcher::Fetcher, tab_store::SnapshotList},
    tui::{
        handler::{ActionDesc, FixedActionDesc, Handler},
        list::{ListData, ListOption},
        screens::resources::get_snapshot_metadata,
        utils::*,
    },
};
use crossterm::event::KeyCode;
use ratatui::{prelude::*, widgets::*};
use similar::TextDiff;
use std::{
    error::Error,
    sync::{
        atomic::{AtomicU16, Ordering},
        Arc, Mutex,
    },
};
use tokio::runtime::Runtime;

pub struct History {
    runtime: Arc<Runtime>,
    fetcher: Arc<Fetcher>,
    tabname_metadataid: Option<(ResourceTabName, String)>,
    scroll_list: Mutex<ListData<LineMeta>>,
    last_height: AtomicU16,
}

#[derive(Clone, PartialEq)]
struct LineMeta {
    /// one for each data_frame
    frame_id: usize,
    /// per line inside a data_frame
    line_id:  usize,
}

impl History {
    pub fn new(
        runtime: Arc<Runtime>,
        fetcher: Arc<Fetcher>,
        tabname_metadataid: Option<(ResourceTabName, String)>,
    ) -> Self {
        Self {
            fetcher,
            runtime,
            tabname_metadataid,
            scroll_list: Mutex::new(ListData::new(vec![], &ListOption::default())),
            last_height: AtomicU16::new(0),
        }
    }

    fn get_text(&self, sl: &SnapshotList) -> Vec<(LineMeta, Line)> {
        let mut selected_history = Vec::new();
        // transform everything in pretty_json
        let pretty_json: Vec<(String, String)> = sl
            .iter()
            .map(|os| {
                (
                    format!("{}", os.snapshot_time.format("%Y-%m-%d %H:%M:%S")),
                    serde_json::to_string_pretty(os.filtered_json.as_ref().unwrap_or(&os.object.data))
                        .unwrap_or("STRINGIFY error".to_string()),
                )
            })
            .collect();
        // fully print first, then only diffs
        if let Some((time, first)) = pretty_json.first() {
            selected_history.push((
                LineMeta {
                    frame_id: 0,
                    line_id:  0,
                },
                Line::styled(time.to_string(), THEME.history.time),
            ));
            for (line_id, line) in first.lines().enumerate() {
                selected_history.push((
                    LineMeta {
                        frame_id: 0,
                        line_id:  line_id + 1,
                    },
                    Line::styled(format!(" |{}", line), THEME.history.line),
                ));
            }
        }
        for (frame_id, w) in pretty_json.windows(2).enumerate() {
            let old = &w[0];
            let new = &w[1];
            let diff = TextDiff::from_lines(&old.1, &new.1);
            let stringified = diff.unified_diff().header(&old.0, &new.0).to_string();
            for (line_id, line) in stringified.lines().enumerate() {
                let line = if line.starts_with('-') {
                    Line::styled(line.to_string(), THEME.history.removed)
                } else if line.starts_with("+++") {
                    Line::styled(line.to_string(), THEME.history.time)
                } else if line.starts_with('+') {
                    Line::styled(line.to_string(), THEME.history.added)
                } else {
                    Line::styled(line.to_string(), THEME.history.line)
                };
                selected_history.push((
                    LineMeta {
                        line_id,
                        frame_id: frame_id + 1,
                    },
                    line,
                ));
            }
        }
        selected_history
    }
}

impl Widget for &History {
    fn render(self, area: Rect, buf: &mut Buffer) {
        self.last_height.store(area.height - 2, Ordering::SeqCst);
        if let Some((name, id)) = &self.tabname_metadataid {
            let binding = self.fetcher.store();
            let store = self.runtime.block_on(binding.read());

            let res = store.get(name);
            if let Some(res) = res {
                if let Some(sl) = res.all().get(id) {
                    let meta = get_snapshot_metadata(sl);

                    let block = Block::default()
                        .title(meta.name)
                        .borders(Borders::all())
                        .border_style(THEME.borders);

                    let text = self.get_text(sl);
                    let mut scroll_list = self.scroll_list.lock().unwrap();
                    scroll_list.update_elements_inplace(text.iter().map(|t| t.0.clone()).collect());

                    Paragraph::new(text.iter().map(|t| t.1.clone()).collect::<Vec<_>>())
                        .block(block)
                        .alignment(Alignment::Left)
                        .wrap(Wrap { trim: true })
                        .scroll((scroll_list.selected_index().unwrap_or_default() as u16, 0))
                        .render(area, buf);
                }
            }
        }
    }
}

#[derive(Clone)]
pub enum HistoryAction {
    Close,
    Up,
    Down,
    PgUp,
    PgDown,
}

#[derive(Clone)]
pub enum HistoryResult {
    Close,
}

impl Handler for History {
    type Action = HistoryAction;
    type Error = Box<dyn Error>;
    type Result = Option<HistoryResult>;

    fn promoted_actions(&self) -> Vec<ActionDesc<Self::Action>> {
        let close = FixedActionDesc {
            action:      HistoryAction::Close,
            events:      vec![(simple_key(KeyCode::Esc), "ESC")],
            title:       "close history",
            description: "closes this history view",
        }
        .into();

        let up = FixedActionDesc {
            action:      HistoryAction::Up,
            events:      vec![(simple_key(KeyCode::Up), "↑")],
            title:       "up",
            description: "scroll up",
        }
        .into();

        let down = FixedActionDesc {
            action:      HistoryAction::Down,
            events:      vec![(simple_key(KeyCode::Down), "↓")],
            title:       "down",
            description: "scroll down",
        }
        .into();

        let page_up = FixedActionDesc {
            action:      HistoryAction::PgUp,
            events:      vec![(simple_key(KeyCode::PageUp), "PgUp")],
            title:       "",
            description: "scroll up by a whole page",
        }
        .into();

        let page_down = FixedActionDesc {
            action:      HistoryAction::PgDown,
            events:      vec![(simple_key(KeyCode::PageDown), "PgDn")],
            title:       "",
            description: "scroll down by a whole page",
        }
        .into();
        vec![close, up, down, page_up, page_down]
    }

    fn handle_action(&mut self, action: &mut Self::Action) -> Result<Self::Result, Self::Error> {
        let mut scroll_list = self.scroll_list.lock().unwrap();
        match action {
            HistoryAction::Close => return Ok(Some(HistoryResult::Close)),
            HistoryAction::Up => scroll_list.previous(),
            HistoryAction::Down => scroll_list.next(),
            HistoryAction::PgUp => {
                for _ in 0..self.last_height.load(Ordering::SeqCst) {
                    scroll_list.previous()
                }
            },
            HistoryAction::PgDown => {
                for _ in 0..self.last_height.load(Ordering::SeqCst) {
                    scroll_list.next()
                }
            },
        }
        Ok(None)
    }
}

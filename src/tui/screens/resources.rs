use crate::{
    config::{AdditionalColumn, ResourceTab, ResourceTabName},
    k8s::{
        clients::Clients,
        discovery::Discovery,
        fetcher::Fetcher,
        tab_store::{ResourceStore, SnapshotList},
    },
    tui::{
        handler::{ActionDesc, FixedActionDesc, Handler},
        list::{ListData, ListOption},
        map_actions,
        screens::{Editor, EditorAction, EditorResult, History, HistoryAction, HistoryResult},
        utils::*,
    },
};
use chrono::{
    format::{DelayedFormat, StrftimeItems},
    Utc,
};
use crossterm::event::KeyCode;
use jsonpath_rust::{parser::model::JsonPath, JsonPathValue};
use k8s_openapi::apiextensions_apiserver::pkg::apis::apiextensions::v1::CustomResourceDefinition;
use kube::{
    api::{DeleteParams, GroupVersionKind, PostParams},
    ResourceExt,
};
use ratatui::{prelude::*, widgets::*};
use serde_json::Value;
use std::{
    error::Error,
    fmt::{Display, Formatter},
    sync::{Arc, Mutex},
};
use tokio::runtime::Runtime;

#[derive(PartialEq)]
struct GroupVersionKindDisplay(GroupVersionKind);

impl From<GroupVersionKind> for GroupVersionKindDisplay {
    fn from(value: GroupVersionKind) -> Self { GroupVersionKindDisplay(value) }
}

impl Display for GroupVersionKindDisplay {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result { write!(f, "{}", self.0.kind) }
}

pub struct Resources {
    runtime:      Arc<Runtime>,
    fetcher:      Arc<Fetcher>,
    clients:      Arc<Clients>,
    discovery:    Arc<Mutex<Discovery>>,
    history:      History,
    show_history: bool,
    editor:       Editor,
    show_editor:  bool,
    tab_list:     Mutex<ListData<ResourceTabName>>,
    res_list:     Mutex<ListData<String>>,
}

impl Resources {
    pub fn new(
        runtime: Arc<Runtime>,
        fetcher: Arc<Fetcher>,
        clients: Arc<Clients>,
        discovery: Arc<Mutex<Discovery>>,
    ) -> Self {
        let options = ListOption {
            always_one: true,
            ..Default::default()
        };
        Self {
            history: History::new(Arc::clone(&runtime), Arc::clone(&fetcher), None),
            editor: Editor::new(
                Arc::clone(&runtime),
                Arc::clone(&fetcher),
                Arc::clone(&clients),
                Arc::clone(&discovery),
                None,
            ),
            fetcher,
            runtime,
            clients,
            discovery,
            show_history: false,
            show_editor: false,
            tab_list: Mutex::new(ListData::new(vec![], &options)),
            res_list: Mutex::new(ListData::new(vec![], &options)),
        }
    }

    fn table_content(&self, resource_store: &ResourceStore) -> (Vec<String>, Vec<Row>, Vec<RowData>, TableState) {
        let mut list: Vec<_> = resource_store.all().iter().collect();
        list.sort_by_key(|(key, sl)| sl.last().map(|l| l.object.name_any()).unwrap_or(format!("ZZZ{key}")));

        let mut res_list = self.res_list.lock().unwrap();
        res_list.update_elements_inplace(list.iter().map(|(k, _)| k).cloned().cloned().collect());

        let mut additional_headers = vec![];
        let extractor = AdditionalColumnsExtractor::new(resource_store.config(), resource_store.crd());

        let (row_data, rows) = list
            .into_iter()
            .map(|(_, sl)| {
                let meta = get_snapshot_metadata(sl);
                let mut row_data: RowData = (&meta).into();
                if let Some(last) = sl.last() {
                    for field in extractor.extract(&last.object.data) {
                        row_data.additional.push(field.1);
                        if !additional_headers.contains(&field.0) {
                            additional_headers.push(field.0);
                        }
                    }
                }
                let name_stlye = match meta.alive {
                    true => Span::raw(meta.name),
                    false => Span::styled(meta.name, THEME.resources.row_deleted),
                };
                let mut row_content = vec![
                    name_stlye,
                    meta.namespace.into(),
                    meta.cnt.to_string().into(),
                    meta.date.to_string().into(),
                ];
                for additional in &row_data.additional {
                    row_content.push(match additional.clone() {
                        Some(additional) => additional.into(),
                        None => "".to_string().into(),
                    });
                }
                (row_data, Row::new(row_content))
            })
            .unzip();

        let default_headers = vec![
            "Name".to_string(),
            "Namespace".to_string(),
            "Cnt".to_string(),
            "Time".to_string(),
        ];
        let mut headers = default_headers;
        headers.append(&mut additional_headers);

        let table_state = res_list.tablestate();

        (headers, rows, row_data, table_state)
    }

    fn table_constrains(&self, area: Rect, metas: Vec<RowData>, headers: &[String]) -> Vec<Constraint> {
        let widths = metas
            .iter()
            .map(|r| {
                let mut widths = vec![
                    r.name.len(),
                    r.namespace.len(),
                    r.cnt.to_string().len(),
                    r.date.to_string().len(),
                ];
                for additional in &r.additional {
                    widths.push(additional.as_ref().map(|a| a.len()).unwrap_or_default());
                }
                widths
            })
            .collect::<Vec<_>>();

        let mut max_widths = Vec::new();
        for row in widths {
            for (i, w) in row.iter().enumerate() {
                match max_widths.get_mut(i) {
                    None => max_widths.push(*w),
                    Some(max) => {
                        if w > max {
                            *max = *w
                        }
                    },
                }
            }
        }

        // make sure at least the header name fits
        for (i, h) in headers.iter().enumerate() {
            if let Some(e) = max_widths.get_mut(i) {
                *e = (*e).max(h.len());
            }
        }

        // adding a bit of space to name for convenience
        if let Some(e) = max_widths.get_mut(0) {
            if *e < 35 {
                *e += 6;
            }
        }

        // add lines plus 2 spacing per element
        let total_width =
            |max_widths: &[usize]| max_widths.iter().sum::<usize>() + max_widths.len().saturating_sub(1) * 2;

        // if to much, reduce CRDs down to 30
        for _ in 0..15 {
            let to_much: usize = total_width(&max_widths).saturating_sub(area.width as usize);
            if to_much == 0 {
                break;
            }
            for w in &mut max_widths[4..] {
                if *w > 50 {
                    *w = w.saturating_sub(6)
                } else if *w > 20 {
                    *w = w.saturating_sub(2)
                }
            }
        }

        // if still to much, limit name
        let to_much: usize = total_width(&max_widths).saturating_sub(area.width as usize);
        if to_much > 0 && max_widths[0] > 20 {
            max_widths[0] = max_widths[0].saturating_sub(to_much).max(20);
        }

        max_widths.iter().map(|c| Constraint::Length(*c as u16)).collect()
    }
}

impl Widget for &Resources {
    fn render(self, area: Rect, buf: &mut Buffer) {
        let binding = self.fetcher.store();
        let tab_names = {
            // to prevent deadlock
            let store = self.runtime.block_on(binding.read());
            store.get_resource_tab_names()
        };

        let mut tab_list = self.tab_list.lock().unwrap();
        tab_list.update_elements_inplace(tab_names.into_iter().collect());

        let tab_layout = Layout::default()
            .direction(Direction::Vertical)
            .constraints([Constraint::Length(1), Constraint::Length(1), Constraint::Min(0)])
            .split(area);

        let tabitems: Vec<_> = tab_list.lineitems(THEME.resources.tab_default);

        let mut tuitabs = Tabs::new(tabitems)
            .style(THEME.resources.tab_background)
            .highlight_style(THEME.resources.tab_highligted);
        if let Some(index) = tab_list.selected_index() {
            tuitabs = tuitabs.select(index);
        }

        ratatui::widgets::Widget::render(tuitabs, tab_layout[0], buf);

        Block::default()
            .borders(Borders::TOP)
            .border_style(THEME.borders)
            .render(tab_layout[1], buf);

        if self.show_history {
            self.history.render(tab_layout[2], buf)
        } else if self.show_editor {
            self.editor.render(tab_layout[2], buf)
        } else if let Some(tab_name) = tab_list.selected() {
            let store = self.runtime.block_on(binding.read());
            if let Some(resource_store) = store.get(tab_name) {
                let mut content = self.table_content(resource_store);
                let constrains = self.table_constrains(area, content.2, &content.0);

                let table = Table::new(content.1, constrains)
                    .style(THEME.resources.row_default)
                    .header(Row::new(content.0).style(THEME.resources.row_header))
                    .column_spacing(2)
                    .highlight_style(THEME.resources.row_highligted);

                ratatui::widgets::StatefulWidget::render(table, tab_layout[2], buf, &mut content.3);

                let mut res_list = self.res_list.lock().unwrap();
                res_list.update_from_tablestate(content.3);
            }
        }
    }
}

#[derive(Clone)]
pub enum ResourcesAction {
    History,
    Edit,
    Forget,
    Delete,
    Recreate,
    Left,
    Right,
    Up,
    Down,
}

#[derive(Clone)]
pub enum ResourcesNestedAction {
    Resources(ResourcesAction),
    History(HistoryAction),
    Editor(EditorAction),
}

impl Handler for Resources {
    type Action = ResourcesNestedAction;
    type Error = Box<dyn Error>;
    type Result = ();

    fn promoted_actions(&self) -> Vec<ActionDesc<Self::Action>> {
        let left = FixedActionDesc {
            action:      ResourcesNestedAction::Resources(ResourcesAction::Left),
            events:      vec![(simple_key(KeyCode::Left), "←")],
            title:       "left",
            description: "selects resource tab to the left",
        }
        .into();

        let right = FixedActionDesc {
            action:      ResourcesNestedAction::Resources(ResourcesAction::Right),
            events:      vec![(simple_key(KeyCode::Right), "→")],
            title:       "right",
            description: "selects resource tab to the right",
        }
        .into();

        let up = FixedActionDesc {
            action:      ResourcesNestedAction::Resources(ResourcesAction::Up),
            events:      vec![(simple_key(KeyCode::Up), "↑")],
            title:       "up",
            description: "selects the upper resource",
        }
        .into();

        let down = FixedActionDesc {
            action:      ResourcesNestedAction::Resources(ResourcesAction::Down),
            events:      vec![(simple_key(KeyCode::Down), "↓")],
            title:       "down",
            description: "selects the lower resource",
        }
        .into();

        let history = FixedActionDesc {
            action:      ResourcesNestedAction::Resources(ResourcesAction::History),
            events:      vec![(simple_key(KeyCode::Enter), "ENTER")],
            title:       "history",
            description: "shows history of resource",
        }
        .into();

        let editor = FixedActionDesc {
            action:      ResourcesNestedAction::Resources(ResourcesAction::Edit),
            events:      vec![(simple_key(KeyCode::Char('e')), "e")],
            title:       "edit",
            description: "modify the resource via editor",
        }
        .into();

        let forget = FixedActionDesc {
            action:      ResourcesNestedAction::Resources(ResourcesAction::Forget),
            events:      vec![(simple_key(KeyCode::Char('f')), "f")],
            title:       "forget",
            description: "forget data of this resource",
        }
        .into();

        let delete = FixedActionDesc {
            action:      ResourcesNestedAction::Resources(ResourcesAction::Delete),
            events:      vec![(simple_key(KeyCode::Delete), "DEL")],
            title:       "delete",
            description: "delete resource from cluster",
        }
        .into();

        let recreate = FixedActionDesc {
            action:      ResourcesNestedAction::Resources(ResourcesAction::Recreate),
            events:      vec![(simple_key(KeyCode::Char('r')), "r")],
            title:       "recreate",
            description: "recreates a resource from cluster if already deleted",
        }
        .into();

        if self.show_history {
            map_actions(self.history.promoted_actions(), ResourcesNestedAction::History)
        } else if self.show_editor {
            map_actions(self.editor.promoted_actions(), ResourcesNestedAction::Editor)
        } else {
            vec![left, right, up, down, history, editor, forget, delete, recreate]
        }
    }

    fn handle_action(&mut self, action: &mut Self::Action) -> Result<Self::Result, Self::Error> {
        let mut tab_list = self.tab_list.lock().unwrap();
        let mut res_list = self.res_list.lock().unwrap();
        match action {
            ResourcesNestedAction::Resources(ResourcesAction::Left) => tab_list.previous(),
            ResourcesNestedAction::Resources(ResourcesAction::Right) => tab_list.next(),
            ResourcesNestedAction::Resources(ResourcesAction::Up) => res_list.previous(),
            ResourcesNestedAction::Resources(ResourcesAction::Down) => res_list.next(),
            ResourcesNestedAction::Resources(ResourcesAction::History) => {
                let tab_name = tab_list.selected().cloned();
                let res = res_list.selected().cloned();
                if let (Some(tab_name), Some(res)) = (tab_name, res) {
                    self.show_history = true;
                    self.history = History::new(
                        Arc::clone(&self.runtime),
                        Arc::clone(&self.fetcher),
                        Some((tab_name, res)),
                    );
                }
            },
            ResourcesNestedAction::Resources(ResourcesAction::Edit) => {
                let tab_name = tab_list.selected().cloned();
                let res = res_list.selected().cloned();
                if let (Some(tab_name), Some(res)) = (tab_name, res) {
                    self.show_editor = true;
                    self.editor = Editor::new(
                        Arc::clone(&self.runtime),
                        Arc::clone(&self.fetcher),
                        Arc::clone(&self.clients),
                        Arc::clone(&self.discovery),
                        Some((tab_name, res)),
                    );
                }
            },
            ResourcesNestedAction::Resources(ResourcesAction::Forget) => {
                let binding = self.fetcher.store();
                let mut store = self.runtime.block_on(binding.write());
                if let Some(tab_name) = tab_list.selected().cloned() {
                    if let Some(sl) = store.get_mut(&tab_name) {
                        if let Some(res) = res_list.selected().cloned() {
                            sl.forget(&res);
                            res_list.next();
                        }
                    }
                }
            },
            ResourcesNestedAction::Resources(ResourcesAction::Delete) => {
                let binding = self.fetcher.store();
                let store = self.runtime.block_on(binding.read());
                if let (Some(tab_name), Some(res)) = (tab_list.selected().cloned(), res_list.selected().cloned()) {
                    if let Some(rs) = store.get(&tab_name) {
                        if let (Some(config), Some(sl)) = (rs.config(), rs.all().get(&res)) {
                            if let Some(last) = sl.last() {
                                let client = self
                                    .clients
                                    .get(&last.snapshot_client)
                                    .ok_or("Could not get client to delete")?;
                                let mut discovery = self.discovery.lock().unwrap();
                                if let Some((api, _)) = self.runtime.block_on(discovery.specific_api(
                                    &config.group_kind_version,
                                    &last.object.metadata,
                                    &client,
                                )) {
                                    let name = last.object.name_any();
                                    self.runtime.block_on(api.delete(&name, &DeleteParams::background()))?;
                                }
                                res_list.next();
                            }
                        }
                    }
                }
            },
            ResourcesNestedAction::Resources(ResourcesAction::Recreate) => {
                let binding = self.fetcher.store();
                let store = self.runtime.block_on(binding.read());
                if let (Some(tab_name), Some(res)) = (tab_list.selected().cloned(), res_list.selected().cloned()) {
                    if let Some(rs) = store.get(&tab_name) {
                        if let (Some(config), Some(sl)) = (rs.config(), rs.all().get(&res)) {
                            if let Some(last) = sl.last() {
                                let client = self
                                    .clients
                                    .get(&last.snapshot_client)
                                    .ok_or("Could not get client to recreate")?;
                                let mut discovery = self.discovery.lock().unwrap();
                                if let Some((api, _)) = self.runtime.block_on(discovery.specific_api(
                                    &config.group_kind_version,
                                    &last.object.metadata,
                                    &client,
                                )) {
                                    let mut obj = last.object.clone();
                                    if obj.metadata.deletion_timestamp.is_some() {
                                        obj.metadata.deletion_timestamp = None;
                                        obj.metadata.resource_version = None;
                                        self.runtime.block_on(api.create(&PostParams::default(), &obj))?;
                                    }
                                }
                            }
                        }
                    }
                }
            },
            #[allow(clippy::single_match)]
            ResourcesNestedAction::History(a) => match self.history.handle_action(a)? {
                Some(HistoryResult::Close) => self.show_history = false,
                None => (),
            },
            #[allow(clippy::single_match)]
            ResourcesNestedAction::Editor(a) => match self.editor.handle_action(a)? {
                Some(EditorResult::Close) => self.show_editor = false,
                None => (),
            },
        };
        Ok(())
    }
}

pub(crate) struct SnapShotMetadata {
    pub name:      String,
    pub namespace: String,
    pub alive:     bool,
    pub date:      DelayedFormat<StrftimeItems<'static>>,
    pub cnt:       usize,
}

pub(crate) fn get_snapshot_metadata(sl: &SnapshotList) -> SnapShotMetadata {
    let (name, namespace, last, alive) = match sl.last() {
        Some(os) => (
            os.object.name_any(),
            os.object.namespace().unwrap_or("NO-NAMESPACE".to_string()),
            os.snapshot_time,
            os.object.metadata.deletion_timestamp.is_none(),
        ),
        None => ("ERROR".to_string(), "NO-NAMESPACE".to_string(), Utc::now(), false),
    };
    let now = Utc::now();
    let date = if last.date_naive() == now.date_naive() {
        last.format("%T")
    } else {
        last.format("%F %T")
    };
    let cnt = sl.len();
    SnapShotMetadata {
        name,
        namespace,
        alive,
        date,
        cnt,
    }
}

#[derive(Clone)]
pub(crate) struct RowData {
    pub name:       String,
    pub namespace:  String,
    pub date:       String,
    pub cnt:        usize,
    pub additional: Vec<Option<String>>,
}

impl From<&SnapShotMetadata> for RowData {
    fn from(value: &SnapShotMetadata) -> Self {
        RowData {
            name:       value.name.clone(),
            namespace:  value.namespace.clone(),
            date:       value.date.to_string(),
            cnt:        value.cnt,
            additional: vec![],
        }
    }
}

#[derive(Clone)]
pub(crate) struct AdditionalColumnsExtractor {
    pub columns: Vec<(String, JsonPath)>,
}

impl AdditionalColumnsExtractor {
    pub fn new(config: &Option<ResourceTab>, crd: &Option<CustomResourceDefinition>) -> Self {
        let mut keys = vec![];
        if let Some(config) = config {
            keys.append(&mut config.additional_columns.clone())
        }

        if let Some(crd) = &crd {
            if let Some(version) = crd.spec.versions.last() {
                if let Some(additional_printer_columns) = version.additional_printer_columns.as_ref() {
                    for adp in additional_printer_columns {
                        keys.push(AdditionalColumn {
                            name:      adp.name.clone(),
                            json_path: adp.json_path.clone(),
                        });
                    }
                }
            }
        }

        keys.sort_by(|a, b| a.json_path.cmp(&b.json_path));
        keys.dedup_by(|a, b| a.json_path == b.json_path);

        let columns = keys
            .into_iter()
            .filter_map(|ac| match ac.json_path.as_str().try_into() {
                Ok(path) => Some((ac.name, path)),
                Err(e) => {
                    tracing::error!(?e, "error parsing jsonPath '{}', fix your config", &ac.json_path);
                    None
                },
            })
            .collect();
        Self { columns }
    }

    fn extract(&self, json: &Value) -> Vec<(/* key */ String, /* value */ Option<String>)> {
        use jsonpath_rust::path::json_path_instance;

        self.columns
            .iter()
            .map(|(name, path)| {
                let instance = json_path_instance(path, json);
                let values = instance.find(JsonPathValue::from_root(json));

                let value = values.into_iter().find(|v| v.has_value()).map(|v| {
                    v.to_data()
                        .to_string()
                        .trim_start_matches('"')
                        .trim_end_matches('"')
                        .to_string()
                });
                (name.clone(), value)
            })
            .collect()
    }
}

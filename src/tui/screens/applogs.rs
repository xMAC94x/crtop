use crate::tui::{
    handler::{ActionDesc, Handler},
    utils::*,
};
use ratatui::{prelude::*, widgets::*};
use std::error::Error;

#[derive(Default)]
pub struct AppLogs {}

impl Widget for &AppLogs {
    fn render(self, area: Rect, buf: &mut Buffer) {
        let scroll = crate::LOG.drain(area.height as usize);
        let inner = crate::LOG.inner.lock().unwrap().clone();

        Paragraph::new(inner)
            .wrap(Wrap { trim: true })
            .scroll((scroll, 0))
            .render(area, buf);
    }
}

impl Handler for Resources {
    type Action = ();
    type Error = Box<dyn Error>;
    type Result = ();

    fn promoted_actions(&self) -> Vec<ActionDesc<Self::Action>> { vec![] }

    fn handle_action(&mut self, _: &mut Self::Action) -> Result<Self::Result, Self::Error> { Ok(()) }
}

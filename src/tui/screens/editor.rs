use crate::{
    config::ResourceTabName,
    k8s::{clients::Clients, discovery::Discovery, fetcher::Fetcher, tab_store::SnapshotList},
    tui::{
        handler::{ActionDesc, DynamicActionDesc, FixedActionDesc, Handler},
        utils::*,
    },
};
use crossterm::event::{Event, KeyCode, KeyEvent, KeyModifiers};
use kube::{api::PostParams, ResourceExt};
use ratatui::prelude::*;
use std::{
    error::Error,
    sync::{Arc, Mutex},
};
use tokio::runtime::Runtime;
use tui_textarea::TextArea;

pub struct Editor {
    runtime: Arc<Runtime>,
    fetcher: Arc<Fetcher>,
    clients: Arc<Clients>,
    discovery: Arc<Mutex<Discovery>>,
    tabname_metadataid: Option<(ResourceTabName, String)>,
    textarea: TextArea<'static>,
}

impl Editor {
    pub fn new(
        runtime: Arc<Runtime>,
        fetcher: Arc<Fetcher>,
        clients: Arc<Clients>,
        discovery: Arc<Mutex<Discovery>>,
        tabname_metadataid: Option<(ResourceTabName, String)>,
    ) -> Self {
        let mut textarea = TextArea::default();

        if let Some((name, id)) = &tabname_metadataid {
            let binding = fetcher.store();
            let store = runtime.block_on(binding.read());

            let res = store.get(name);
            if let Some(res) = res {
                if let Some(sl) = res.all().get(id) {
                    textarea = TextArea::new(Self::get_text(sl));
                }
            }
        }

        Self {
            fetcher,
            runtime,
            clients,
            discovery,
            tabname_metadataid,
            textarea,
        }
    }

    fn get_text(sl: &SnapshotList) -> Vec<String> {
        let mut selected_history = Vec::new();

        if let Some(last) = sl.last() {
            let data = serde_json::to_string_pretty(last.json.as_ref().unwrap_or(&last.object.data))
                .unwrap_or("STRINGIFY error".to_string());
            for line in data.lines() {
                selected_history.push(line.to_string());
            }
        }
        selected_history
    }
}

impl Widget for &Editor {
    fn render(self, area: Rect, buf: &mut Buffer) { self.textarea.widget().render(area, buf) }
}

#[derive(Clone)]
pub enum EditorAction {
    Close,
    Apply,
    EditText(KeyEvent),
}

#[derive(Clone)]
pub enum EditorResult {
    Close,
}

impl Handler for Editor {
    type Action = EditorAction;
    type Error = Box<dyn Error>;
    type Result = Option<EditorResult>;

    fn promoted_actions(&self) -> Vec<ActionDesc<Self::Action>> {
        let close = FixedActionDesc {
            action:      EditorAction::Close,
            events:      vec![(simple_key(KeyCode::Esc), "ESC")],
            title:       "close editor",
            description: "closes this editor view",
        }
        .into();

        let apply = FixedActionDesc {
            action:      EditorAction::Apply,
            events:      vec![(
                Event::Key(KeyEvent::new(KeyCode::Char('s'), KeyModifiers::CONTROL)),
                "CTRL+S",
            )],
            title:       "apply",
            description: "apply changes to cluster",
        }
        .into();

        let edit = DynamicActionDesc {
            try_from_event: Box::new(edit_text),
            title: "<TYPING>",
            description: "type anything to modify the resource",
        }
        .into();

        vec![close, apply, edit]
    }

    fn handle_action(&mut self, action: &mut Self::Action) -> Result<Self::Result, Self::Error> {
        match action {
            EditorAction::Close => Ok(Some(EditorResult::Close)),
            EditorAction::Apply => {
                let binding = self.fetcher.store();
                let store = self.runtime.block_on(binding.read());
                if let Some((tab_name, res)) = self.tabname_metadataid.clone() {
                    if let Some(rs) = store.get(&tab_name) {
                        if let (Some(config), Some(sl)) = (rs.config(), rs.all().get(&res)) {
                            if let Some(last) = sl.last() {
                                let client = self
                                    .clients
                                    .get(&last.snapshot_client)
                                    .ok_or("Could not get client to recreate")?;
                                let mut discovery = self.discovery.lock().unwrap();
                                let lines = self.textarea.lines().join("\n");
                                if let Ok(edited_object) = serde_json::from_str(&lines) {
                                    return self.runtime.block_on(async {
                                        if let Some((api, _)) = discovery
                                            .specific_api(&config.group_kind_version, &last.object.metadata, &client)
                                            .await
                                        {
                                            api.replace(
                                                &last.object.name_any(),
                                                &PostParams::default(),
                                                &edited_object,
                                            )
                                            .await?;
                                        }
                                        Ok(Some(EditorResult::Close))
                                    });
                                }
                            }
                        }
                    }
                }
                Ok(None)
            },
            EditorAction::EditText(e) => {
                self.textarea.input(*e);
                Ok(None)
            },
        }
    }
}

fn edit_text(e: Event) -> Result<EditorAction, Event> {
    if let Event::Key(ke) = e {
        Ok(EditorAction::EditText(ke))
    } else {
        Err(e)
    }
}

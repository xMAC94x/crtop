use crate::tui::{
    handler::{ActionDesc, FixedActionDesc, Handler},
    list::{ListData, ListOption},
    utils::*,
    TopLevelScreen,
};
use crossterm::event::KeyCode;
use ratatui::{prelude::*, widgets::*};
use std::{error::Error, sync::Mutex};

const OPTIONS: [TopLevelScreen; 3] = [
    TopLevelScreen::Resources,
    TopLevelScreen::AppLogs,
    TopLevelScreen::LoadStore,
];

pub struct Menu {
    list: Mutex<ListData<String>>,
}

impl Default for Menu {
    fn default() -> Self {
        let options = ListOption::default();
        let list = ListData::new(OPTIONS.map(|o| o.desc().to_string()).into_iter().collect(), &options);
        Self { list: Mutex::new(list) }
    }
}

impl Widget for &Menu {
    fn render(self, area: Rect, buf: &mut Buffer) {
        let mut list = self.list.lock().unwrap();

        let height = OPTIONS.len() as u16;
        let width = list
            .elements()
            .iter()
            .max_by_key(|e| e.len())
            .map(|e| e.len())
            .unwrap_or_default() as u16;

        const MARGIN: Margin = Margin {
            horizontal: 2,
            vertical:   2,
        };

        let center = centered_rect(area, width + 2 + MARGIN.horizontal * 2, height + MARGIN.vertical * 2);

        let listitems = list.listitems(THEME.menu.default);

        Clear.render(center, buf);
        Block::default()
            .bg(THEME.menu.background)
            .borders(Borders::all())
            .border_style(THEME.borders)
            .title("Menu")
            .render(center, buf);
        let inner_center = center.inner(&MARGIN);

        let tuilist = List::new(listitems)
            .highlight_symbol("> ")
            .highlight_style(THEME.menu.highligted)
            .direction(ListDirection::TopToBottom);

        let mut state = list.liststate();
        ratatui::widgets::StatefulWidget::render(tuilist, inner_center, buf, &mut state);
        list.update_from_liststate(state);
    }
}

#[derive(Clone)]
pub enum MenuAction {
    Close,
    Up,
    Down,
    Select,
}

pub enum MenuResult {
    Close,
    Select(TopLevelScreen),
}

impl Handler for Menu {
    type Action = MenuAction;
    type Error = Box<dyn Error>;
    type Result = Option<MenuResult>;

    fn promoted_actions(&self) -> Vec<ActionDesc<Self::Action>> {
        let close = FixedActionDesc {
            action:      MenuAction::Close,
            events:      vec![(simple_key(KeyCode::Esc), "ESC")],
            title:       "close menu",
            description: "closes this menu",
        }
        .into();

        let up = FixedActionDesc {
            action:      MenuAction::Up,
            events:      vec![(simple_key(KeyCode::Up), "↑")],
            title:       "up",
            description: "selects the upper element",
        }
        .into();

        let down = FixedActionDesc {
            action:      MenuAction::Down,
            events:      vec![(simple_key(KeyCode::Down), "↓")],
            title:       "down",
            description: "selects the lower element",
        }
        .into();

        let select = FixedActionDesc {
            action:      MenuAction::Select,
            events:      vec![(simple_key(KeyCode::Enter), "ENTER")],
            title:       "select",
            description: "selects the element",
        }
        .into();

        vec![close, up, down, select]
    }

    fn handle_action(&mut self, action: &mut Self::Action) -> Result<Self::Result, Self::Error> {
        match action {
            MenuAction::Close => return Ok(Some(MenuResult::Close)),
            MenuAction::Up => self.list.lock().unwrap().previous(),
            MenuAction::Down => self.list.lock().unwrap().next(),
            MenuAction::Select => {
                let list = self.list.lock().unwrap();
                let selected = list.selected();
                for o in OPTIONS {
                    if Some(o.desc()) == selected.map(|s| s.as_str()) {
                        return Ok(Some(MenuResult::Select(o)));
                    }
                }
                return Ok(None);
            },
        };
        Ok(None)
    }
}

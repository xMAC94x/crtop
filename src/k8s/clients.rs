use crate::config::ClientName;
use kube::{
    config::{KubeConfigOptions, Kubeconfig},
    Client,
};
use std::{collections::HashMap, path::Path, sync::RwLock};

pub struct NamedClient {
    pub name:   ClientName,
    pub client: Client,
}

#[derive(Default)]
pub struct Clients {
    default: ClientName,
    clients: RwLock<HashMap<ClientName, Client>>,
}

impl Clients {
    pub fn add_default(&mut self, name: &str, client: Client) {
        self.default = name.into();
        self.clients.write().unwrap().insert(name.into(), client);
    }

    pub async fn load(&self, name: &ClientName, filename: &Path) {
        if let Ok(kubeconfig) = Kubeconfig::read_from(filename) {
            if let Ok(config) = kube::Config::from_custom_kubeconfig(kubeconfig, &KubeConfigOptions::default()).await {
                if let Ok(client) = Client::try_from(config) {
                    self.clients.write().unwrap().insert(name.clone(), client);
                }
            }
        }
    }

    pub fn list(&self) -> Vec<ClientName> { self.clients.read().unwrap().keys().cloned().collect() }

    pub fn get_client(&self, name: &ClientName) -> Option<Client> { self.clients.read().unwrap().get(name).cloned() }

    pub fn get_default(&self) -> Option<NamedClient> {
        let c = self.clients.read().unwrap();
        c.get(&self.default)
            .map(|c| NamedClient {
                name:   self.default.clone(),
                client: c.clone(),
            })
            .or_else(|| {
                c.iter().next().map(|(n, c)| NamedClient {
                    name:   n.clone(),
                    client: c.clone(),
                })
            })
    }

    pub fn get(&self, name: &ClientName) -> Option<NamedClient> {
        self.get_client(name).map(|c| NamedClient {
            name:   name.clone(),
            client: c,
        })
    }

    pub fn get_any(&self, preferred: &Option<ClientName>) -> Option<NamedClient> {
        preferred.as_ref().map(|n| self.get(n)).unwrap_or(self.get_default())
    }
}

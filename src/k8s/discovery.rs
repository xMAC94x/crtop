use crate::{
    config::{ClientName, GroupKindVersion, VersionMode},
    k8s::clients::NamedClient,
};
use k8s_openapi::apimachinery::pkg::apis::meta::v1::ObjectMeta;
use kube::{
    api::{ApiResource, DynamicObject, GroupVersionKind},
    discovery::{ApiCapabilities, ApiGroup, Scope},
    Api,
};
use std::collections::{hash_map::Entry, HashMap};

#[derive(Default)]
pub struct Discovery {
    cache: HashMap<(ClientName, /* Group */ String), ApiGroup>,
}

impl Discovery {
    async fn get_apigroup(&mut self, group: &str, client: &NamedClient) -> Option<&ApiGroup> {
        if let Entry::Vacant(e) = self.cache.entry((client.name.clone(), group.to_string())) {
            tracing::info!(?group, ?client.name, "apigroup not found in cache, getting from cluster");
            match kube::discovery::group(&client.client, group).await {
                Ok(apigroup) => {
                    e.insert(apigroup);
                },
                Err(e) => tracing::error!(?e, "problem discovering ApiResource"),
            }
        };
        self.cache.get(&(client.name.clone(), group.to_string()))
    }

    pub async fn get_resource(
        &mut self,
        group_kind_version: &GroupKindVersion,
        client: &NamedClient,
    ) -> Option<(ApiResource, ApiCapabilities)> {
        if let Some(apigroup) = self.get_apigroup(&group_kind_version.group, client).await {
            match &group_kind_version.version {
                VersionMode::MostStable => apigroup
                    .resources_by_stability()
                    .iter()
                    .find(|(r, _)| r.kind == group_kind_version.kind)
                    .cloned(),
                VersionMode::Specific(version) => apigroup
                    .versioned_resources(version)
                    .iter()
                    .find(|(r, _)| r.kind == group_kind_version.kind)
                    .cloned(),
            }
        } else {
            None
        }
    }

    pub async fn watch_api_and_gvk(
        &mut self,
        group_kind_version: &GroupKindVersion,
        client: &NamedClient,
    ) -> Option<(Api<DynamicObject>, GroupVersionKind)> {
        self.get_resource(group_kind_version, client).await.map(|(res, _)| {
            let api = Api::all_with(client.client.clone(), &res);
            let gvk = GroupVersionKind::gvk(&res.group, &res.version, &res.kind);
            (api, gvk)
        })
    }

    pub async fn specific_api(
        &mut self,
        group_kind_version: &GroupKindVersion,
        metadata: &ObjectMeta,
        client: &NamedClient,
    ) -> Option<(Api<DynamicObject>, GroupVersionKind)> {
        self.get_resource(group_kind_version, client).await.map(|(res, cap)| {
            let api: Api<DynamicObject> = match (cap.scope, &metadata.namespace) {
                (Scope::Cluster, _) => Api::all_with(client.client.clone(), &res),
                (Scope::Namespaced, Some(ns)) => Api::namespaced_with(client.client.clone(), ns, &res),
                (Scope::Namespaced, None) => Api::default_namespaced_with(client.client.clone(), &res),
            };
            let gvk = GroupVersionKind::gvk(&res.group, &res.version, &res.kind);
            (api, gvk)
        })
    }
}

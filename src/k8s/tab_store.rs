use crate::config::{ClientName, ResourceTab, ResourceTabName};
use chrono::{offset::Utc, DateTime};
use k8s_openapi::apiextensions_apiserver::pkg::apis::apiextensions::v1::CustomResourceDefinition;
use kube::core::{DynamicObject, GroupVersionKind};
use serde::{Deserialize, Serialize};
use serde_with::serde_as;
use std::collections::HashMap;

/// Single Frame of an concrete Object of concrete Type
#[derive(Debug, PartialEq, Deserialize, Serialize)]
pub struct ObjectSnapshot {
    pub object: DynamicObject,
    /// to be used rather than .object.data because it also contains metadata
    #[serde(skip)]
    pub json: Option<serde_json::Value>,
    /// pre-generated with ignored_json_paths
    #[serde(skip)]
    pub filtered_json: Option<serde_json::Value>,
    pub snapshot_client: ClientName,
    pub snapshot_time: DateTime<Utc>,
}

pub type SnapshotList = Vec<ObjectSnapshot>;

/// all Frames of an all Objects of concrete Type
#[derive(Debug, Default, PartialEq, Deserialize, Serialize)]
pub struct ResourceStore {
    resources: HashMap</* metadata.uid */ String, SnapshotList>,
    #[serde(skip)]
    gvk:       Option<GroupVersionKind>,
    #[serde(skip)]
    crd:       Option<CustomResourceDefinition>,
    #[serde(skip)]
    config:    Option<ResourceTab>,
}

/// all Frames of an all Objects of concrete Type for a specific Tab
#[serde_as]
#[derive(Debug, Default, PartialEq, Deserialize, Serialize)]
pub struct TabStore {
    #[serde_as(as = "Vec<(_, _)>")]
    pub types: HashMap<ResourceTabName, ResourceStore>,
}

impl ObjectSnapshot {
    pub fn new(object: DynamicObject, client_name: ClientName, config: &Option<ResourceTab>) -> Self {
        let mut obj = Self {
            json: None,
            filtered_json: None,
            object,
            snapshot_client: client_name,
            snapshot_time: Utc::now(),
        };
        obj.regenerate(config);
        obj
    }

    /// necessary after serializing by serde
    fn regenerate(&mut self, config: &Option<ResourceTab>) {
        self.json = serde_json::to_value(&self.object).ok();
        let mut filtered_json = self.json.clone();

        if let Some(config) = config {
            filtered_json = filtered_json.map(|mut filtered_json| {
                'outer: for mut filter in config.ignored_json_paths.clone() {
                    let last = filter.pop();
                    let mut ref_value = filtered_json.as_object_mut();
                    for path in filter {
                        if let Some(r_value) = ref_value {
                            if let Some(value) = r_value.get_mut(&path) {
                                ref_value = value.as_object_mut();
                                continue;
                            }
                        }
                        continue 'outer;
                    }
                    if let Some(last) = last {
                        ref_value.map(|ref_value| ref_value.remove(&last));
                    }
                }
                filtered_json
            });
        }

        self.filtered_json = filtered_json;
    }
}

#[derive(Debug)]
pub enum ResourceError {
    MetaDataIdDoesNotExit,
}

impl ResourceStore {
    pub fn record(&mut self, dynamic_object: DynamicObject, client_name: ClientName) -> Result<(), ResourceError> {
        let uid = dynamic_object
            .metadata
            .uid
            .as_ref()
            .ok_or(ResourceError::MetaDataIdDoesNotExit)?;
        let list = self.resources.entry(uid.clone()).or_default();
        let snapshot = ObjectSnapshot::new(dynamic_object, client_name, &self.config);
        list.push(snapshot);
        Ok(())
    }

    pub fn set_meta(&mut self, gvk: GroupVersionKind, crd: Option<CustomResourceDefinition>, config: ResourceTab) {
        self.gvk = Some(gvk);
        self.crd = crd;
        self.config = Some(config);
    }

    pub fn config(&self) -> &Option<ResourceTab> { &self.config }

    pub fn crd(&self) -> &Option<CustomResourceDefinition> { &self.crd }

    pub fn gvk(&self) -> Option<GroupVersionKind> { self.gvk.clone() }

    pub fn forget(&mut self, metadata_uid: &str) { self.resources.remove(metadata_uid); }

    pub fn all(&self) -> &HashMap<String, SnapshotList> { &self.resources }
}

impl TabStore {
    pub fn resource_mut(&mut self, name: ResourceTabName) -> &mut ResourceStore { self.types.entry(name).or_default() }

    pub fn get_resource_tab_names(&self) -> Vec<ResourceTabName> { self.types.keys().cloned().collect() }

    pub fn get(&self, name: &ResourceTabName) -> Option<&ResourceStore> { self.types.get(name) }

    pub fn get_mut(&mut self, name: &ResourceTabName) -> Option<&mut ResourceStore> { self.types.get_mut(name) }

    pub fn append(&mut self, other: Self) {
        for (k, new_resource_store) in other.types.into_iter() {
            let resource_store = self.types.entry(k).or_default();
            for (k, mut new_snapshot_list) in new_resource_store.resources.into_iter() {
                let snapshot_list = resource_store.resources.entry(k).or_default();
                for o in &mut new_snapshot_list {
                    o.regenerate(&resource_store.config);
                }
                snapshot_list.append(&mut new_snapshot_list);
                snapshot_list.sort_by(|a, b| a.snapshot_time.cmp(&b.snapshot_time));
                snapshot_list.dedup_by(|a, b| a.object == b.object);
            }
        }
    }
}

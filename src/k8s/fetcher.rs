use crate::{
    config::{ClientName, ResourceTab, ResourceTabName, WatchSelector},
    k8s::{clients::Clients, discovery::Discovery, tab_store::TabStore},
};
use futures::TryStreamExt;
use k8s_openapi::apiextensions_apiserver::pkg::apis::apiextensions::v1::CustomResourceDefinition;
use kube::{
    api::{Api, DynamicObject, ListParams},
    core::GroupVersionKind,
    runtime::{watcher, watcher::ListSemantic, WatchStreamExt},
    Client, ResourceExt,
};
use std::{collections::HashMap, sync::Arc};
use tokio::{runtime::Runtime, sync::RwLock, task::JoinHandle};

pub struct Fetcher {
    runtime: Arc<Runtime>,
    store:   Arc<RwLock<TabStore>>,
    handles: HashMap<(/* ResourceTab Name */ ResourceTabName, WatchSelector), JoinHandle<Result<(), watcher::Error>>>,
}

impl Fetcher {
    pub fn new(runtime: Arc<Runtime>) -> Self {
        Self {
            runtime,
            store: Arc::default(),
            handles: HashMap::new(),
        }
    }

    pub fn store(&self) -> Arc<RwLock<TabStore>> { Arc::clone(&self.store) }

    fn watch(
        &mut self,
        resource_name: ResourceTabName,
        client_name: ClientName,
        api: Api<DynamicObject>,
        selector: &WatchSelector,
    ) {
        let watcher_config = watcher::Config {
            label_selector: selector.label_selector.clone(),
            field_selector: selector.field_selector.clone(),
            list_semantic: ListSemantic::Any,
            ..Default::default()
        };

        let store = self.store.clone();
        let resource_name_2 = resource_name.clone();
        let fut = watcher(api, watcher_config).touched_objects().try_for_each(move |k| {
            let store = store.clone();
            let resource_name = resource_name.clone();
            let client_name = client_name.clone();
            async move {
                let mut type_store = store.write().await;
                let name = k.name_any();
                tracing::trace!(?resource_name, ?name, "applied update to");
                if let Err(e) = type_store.resource_mut(resource_name.clone()).record(k, client_name) {
                    tracing::error!(?e, "could not update store");
                };
                Ok(())
            }
        });
        let handle = self.runtime.spawn(fut);
        self.handles.insert((resource_name_2, selector.clone()), handle);
    }

    async fn retrieve_metadata(&mut self, client: Client, gvk: GroupVersionKind, config: ResourceTab) {
        let crds: Api<CustomResourceDefinition> = Api::all(client.clone());

        let crd = match crds.list(&ListParams::default()).await {
            Ok(crds) => crds
                .into_iter()
                .find(|crd| crd.spec.group == gvk.group && crd.spec.names.kind == gvk.kind),
            Err(e) => {
                tracing::error!(?gvk, ?e, "could not find CRD for gvk, ignoring metadata");
                None
            },
        };

        let mut store = self.store.write().await;
        store.resource_mut(config.name.clone()).set_meta(gvk, crd, config);
    }

    pub async fn add_config(&mut self, config: ResourceTab, discovery: &mut Discovery, clients: &Clients) {
        let client = clients.get_any(&config.client_name).unwrap();
        if let Some((api, gvk)) = discovery.watch_api_and_gvk(&config.group_kind_version, &client).await {
            self.retrieve_metadata(client.client.clone(), gvk.clone(), config.clone())
                .await;
            for selector in &config.selector {
                self.watch(config.name.clone(), client.name.clone(), api.clone(), selector);
            }
        }
    }
}

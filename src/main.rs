use anyhow::Context;
use clap::Parser;
use kube::Client;
use std::sync::Arc;
use tokio::runtime::Builder;

use crate::{
    args::Commands,
    config::file::ConfigFile,
    k8s::{clients::Clients, discovery::Discovery, fetcher::Fetcher},
    tui::{trace::TuiLog, AppState},
};

pub mod args;
pub mod config;
pub mod k8s;
pub mod tui;

lazy_static::lazy_static! {
    pub static ref LOG: TuiLog<'static> = TuiLog::default();
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let _guard = tui::trace::init(&|| LOG.clone());
    let cores = std::thread::available_parallelism()
        .map(|c| (c.get() / 2).clamp(2, 4))
        .unwrap_or(4);
    let rt = Arc::new(Builder::new_multi_thread().enable_all().worker_threads(cores).build()?);
    let mut clients = Clients::default();
    let mut discovery = Discovery::default();
    let mut fetcher = Fetcher::new(Arc::clone(&rt));

    let args = args::Cli::parse();
    if !commands(&args) {
        return Ok(());
    }
    rt.block_on(load(args, &mut clients, &mut discovery, &mut fetcher))?;

    let app = tui::App::new(AppState {
        fetcher,
        clients,
        runtime: rt,
        discovery,
    });
    app.run()?;
    Ok(())
}

fn commands(args: &args::Cli) -> bool {
    match args.command {
        Some(Commands::GenerateConfigTemplate) => {
            let config = ConfigFile::generate_default();
            if let Ok(str) = serde_json::to_string_pretty(&config) {
                println!("{}", str);
            }
            false
        },
        None => true,
    }
}

async fn load(
    args: args::Cli,
    clients: &mut Clients,
    discovery: &mut Discovery,
    fetcher: &mut Fetcher,
) -> Result<(), Box<dyn std::error::Error>> {
    use std::path::PathBuf;
    clients.add_default("", Client::try_default().await.unwrap());

    let mut system_files = vec![PathBuf::from("/etc/crtop.conf")];
    if let Some(home) = home::home_dir() {
        let mut config_crtop = home.clone();
        config_crtop.push(".config");
        config_crtop.push("crtop");
        config_crtop.push("config.json");
        system_files.push(config_crtop)
    }

    let config: ConfigFile = if let Some(config_file) = args.config {
        let file = std::fs::File::open(config_file)?;
        let reader = std::io::BufReader::new(file);
        serde_json::from_reader(reader).context("cannot open requested kubeconfig")?
    } else {
        let path = system_files
            .into_iter()
            .rev()
            .find(|p| p.exists())
            .ok_or("No Config File provided, run --help to see how to generate one")?;
        let file = std::fs::File::open(path.clone())?;
        let reader = std::io::BufReader::new(file);
        serde_json::from_reader(reader)
            .context("cannot open requested kubeconfig")
            .context(path.to_string_lossy().to_string())?
    };

    for k in config.kubeconfigs {
        clients.load(&k.0, &k.1).await;
    }
    for w in config.resources {
        fetcher.add_config(w, discovery, clients).await;
    }

    Ok(())
}
